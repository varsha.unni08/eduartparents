package com.report.schoolparents.fragments;


import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.report.schoolparents.AppLiterals.Constants;
import com.report.schoolparents.R;
import com.report.schoolparents.activities.AddLeaveActivity;
import com.report.schoolparents.activities.AttendanceActivity;
import com.report.schoolparents.activities.LessonPlanActivity;
import com.report.schoolparents.adapters.LeaveAdapter;
import com.report.schoolparents.connectivity.NetConnection;
import com.report.schoolparents.pojo.Leave;
import com.report.schoolparents.pojo.LeaveStatus;
import com.report.schoolparents.pojo.Student;
import com.report.schoolparents.pojo.StudentProfile;
import com.report.schoolparents.pojo.StudentProfileStatus;
import com.report.schoolparents.progress.ProgressFragment;
import com.report.schoolparents.webservice.RetrofitHelper;
import com.report.schoolparents.webservice.WebInterfaceimpl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class LeaveFragment extends Fragment {


    public LeaveFragment() {
        // Required empty public constructor
    }

    TextView txtNodata,txtStudentSelection;

    RecyclerView recyclerview;

    View view;

    ProgressFragment progressFragment;

    ImageView imgselectstudent;

    String studentid="";

    FloatingActionButton fabAdd;

    ImageView imgdate;
    TextView txtdate;
    Button btnShowLeave;

    int month=0,year=0;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_leave, container, false);
        txtNodata=view.findViewById(R.id.txtNodata);
        recyclerview=view.findViewById(R.id.recyclerview);
        txtStudentSelection=view.findViewById(R.id.txtStudentSelection);
        imgselectstudent=view.findViewById(R.id.imgselectstudent);
        fabAdd=view.findViewById(R.id.fabAdd);

        imgdate =view. findViewById(R.id.imgdate);
        txtdate = view.findViewById(R.id.txtdate);
        btnShowLeave=view.findViewById(R.id.btnShowLeave);

        btnShowLeave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(month!=0&&year!=0)
                {

                        getLeaveList(studentid);

                }
                else {

                    Toast.makeText(getActivity(),"Select month and year",Toast.LENGTH_SHORT).show();
                }

            }
        });

        if(Constants.studentprofile!=null)
        {
            txtStudentSelection.setText("Selected student : "+Constants.studentprofile.getUdcStudentName());

            studentid=Constants.studentprofile.getUdcStudentId();
        }

//        txtStudentSelection.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                getStudentList();
//
//            }
//        });

//        imgselectstudent.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                getStudentList();
//            }
//        });


        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(getActivity(), AddLeaveActivity.class));

            }
        });


        imgdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ExpiryDialog();

            }
        });

        txtdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ExpiryDialog();

            }
        });



        return view;
    }





    private void ExpiryDialog() {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_date_picker);
        dialog.show();

        Calendar mCalendar = Calendar.getInstance();

        final DatePicker datePicker = (DatePicker) dialog.findViewById(R.id.date_picker);
        Button date_time_set = (Button) dialog.findViewById(R.id.date_time_set);
        datePicker.init(mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH), null);

        datePicker.setSpinnersShown(true);

        // hiding calendarview and daySpinner in datePicker
        datePicker.setCalendarViewShown(false);

        LinearLayout pickerParentLayout = (LinearLayout) datePicker.getChildAt(0);

        LinearLayout pickerSpinnersHolder = (LinearLayout) pickerParentLayout.getChildAt(0);

        pickerSpinnersHolder.getChildAt(1).setVisibility(View.GONE);

//        LinearLayout ll = (LinearLayout) datePicker.getChildAt(0);
//        LinearLayout ll2 = (LinearLayout) ll.getChildAt(0);
//        ll2.getChildAt(0).setVisibility(View.INVISIBLE);

        date_time_set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();


                try {
                    month = datePicker.getMonth() + 1;
                    year = datePicker.getYear();

                    String expMonth = (month/10 >= 1 ? "0" + month : month+"");
                    String expYear = year+"";
                    txtdate.setText(expMonth + "/" + expYear);


                } catch (Exception e) {

                }

            }
        });
    }

    public void getLeaveList(String studentid) {

        progressFragment = new ProgressFragment();
        progressFragment.show(getFragmentManager(), "cjkk");

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(getActivity());

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

        Call<LeaveStatus>listCall=webInterfaceimpl.getAllLeaves(studentid,month+"",year+"");

        listCall.enqueue(new Callback<LeaveStatus>() {
            @Override
            public void onResponse(Call<LeaveStatus> call, Response<LeaveStatus> response) {

                progressFragment.dismiss();

                if(response.body()!=null)
                {
                    if(response.body().getData().size()>0)
                    {

                        recyclerview.setVisibility(View.VISIBLE);
                        txtNodata.setVisibility(View.GONE);

                        recyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
                        recyclerview.setAdapter(new LeaveAdapter(getActivity(),response.body().getData()));




                    }
                    else {

                        recyclerview.setVisibility(View.GONE);
                        txtNodata.setVisibility(View.VISIBLE);
                    }


                }

                else {

                    recyclerview.setVisibility(View.GONE);
                    txtNodata.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onFailure(Call<LeaveStatus> call, Throwable t) {
                progressFragment.dismiss();


                if(!NetConnection.isConnected(getActivity()))
                {
                    Toast.makeText(getActivity(),"Check Internet connection",Toast.LENGTH_SHORT).show();
                }

            }
        });

    }



    public void getStudentList()
    {

        progressFragment=new ProgressFragment();
        progressFragment.show(getChildFragmentManager(),"cjkk");

        Retrofit retrofitHelper= RetrofitHelper.getRetrofitInstance(getActivity());

        WebInterfaceimpl webInterfaceimpl=retrofitHelper.create(WebInterfaceimpl.class);

        Call<StudentProfileStatus> listCall=webInterfaceimpl.getStudentProfile();
        listCall.enqueue(new Callback<StudentProfileStatus>() {
            @Override
            public void onResponse(Call<StudentProfileStatus> call, Response<StudentProfileStatus> response) {

                progressFragment.dismiss();

                if(response.body()!=null) {

                    if(response.body().getData().size()>0) {

                        showStudentList(response.body().getData());
                    }
                    else {

                        Toast.makeText(getActivity(),"No data found",Toast.LENGTH_SHORT).show();

                    }

                }

                else {

                    Toast.makeText(getActivity(),"No data found",Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<StudentProfileStatus> call, Throwable t) {

                progressFragment.dismiss();


                if(!NetConnection.isConnected(getActivity()))
                {
                    Toast.makeText(getActivity(),"Check Internet connection",Toast.LENGTH_SHORT).show();
                }


            }
        });


    }


    public void showStudentList(final List<StudentProfile>students)
    {

        List<String>strings=new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose a student");
        ;

        for (StudentProfile student:students
        ) {

            strings.add(student.getUdcStudentName());

        }



        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtStudentSelection.setText(students.get(which).getUdcStudentName());

                studentid=students.get(which).getUdcStudentId();



            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

}
