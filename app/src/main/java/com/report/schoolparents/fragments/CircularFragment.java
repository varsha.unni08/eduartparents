package com.report.schoolparents.fragments;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.report.schoolparents.R;
import com.report.schoolparents.adapters.CircularAdapter;
import com.report.schoolparents.connectivity.NetConnection;
import com.report.schoolparents.pojo.Circular;
import com.report.schoolparents.pojo.CircularStatus;
import com.report.schoolparents.progress.ProgressFragment;
import com.report.schoolparents.webservice.RetrofitHelper;
import com.report.schoolparents.webservice.WebInterfaceimpl;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class CircularFragment extends Fragment {


    public CircularFragment() {
        // Required empty public constructor
    }

    View view;

    TextView txtNodata;
    RecyclerView recycler_view;

    ProgressFragment progressFragment;

    List<Circular>circulars;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_circular, container, false);
        recycler_view=view.findViewById(R.id.recycler_view);
        txtNodata=view.findViewById(R.id.txtNodata);
        circulars=new ArrayList<>();



        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        getAllCircular();
    }

    public void getAllCircular()
    {
        if(circulars.size()==0) {


            progressFragment = new ProgressFragment();
            progressFragment.show(getFragmentManager(), "cjkk");

            Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(getActivity());

            WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

            Call<CircularStatus> listCall = webInterfaceimpl.getAllCirculars();

            listCall.enqueue(new Callback<CircularStatus>() {
                @Override
                public void onResponse(Call<CircularStatus> call, Response<CircularStatus> response) {

                    progressFragment.dismiss();


                    if (response.body() != null) {

                        if(response.body().getStatus()==1) {

                            if (response.body().getData().size() > 0) {

                                recycler_view.setVisibility(View.VISIBLE);
                                txtNodata.setVisibility(View.GONE);

                                circulars.addAll(response.body().getData());

                                recycler_view.setLayoutManager(new LinearLayoutManager(getActivity()));

                                recycler_view.setAdapter(new CircularAdapter(getActivity(), circulars));


                            } else {
                                // Toast.makeText(getActivity(),"No data found",Toast.LENGTH_SHORT).show();

                                recycler_view.setVisibility(View.GONE);
                                txtNodata.setVisibility(View.VISIBLE);

                            }
                        }
                        else {

                            recycler_view.setVisibility(View.GONE);
                            txtNodata.setVisibility(View.VISIBLE);

                        }


                    } else {
                        // Toast.makeText(getActivity(),"No data found",Toast.LENGTH_SHORT).show();

                        recycler_view.setVisibility(View.GONE);
                        txtNodata.setVisibility(View.VISIBLE);
                    }

                }

                @Override
                public void onFailure(Call<CircularStatus> call, Throwable t) {
                    progressFragment.dismiss();

                    recycler_view.setVisibility(View.GONE);
                    txtNodata.setVisibility(View.VISIBLE);


                    if (!NetConnection.isConnected(getActivity())) {
                        Toast.makeText(getActivity(), "Check Internet connection", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

}
