package com.report.schoolparents.fragments;


import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.report.schoolparents.AppLiterals.Constants;
import com.report.schoolparents.R;
import com.report.schoolparents.adapters.MessageAdapter;
import com.report.schoolparents.connectivity.NetConnection;
import com.report.schoolparents.pojo.Message;
import com.report.schoolparents.pojo.MessageStatus;
import com.report.schoolparents.pojo.Student;
import com.report.schoolparents.pojo.StudentProfile;
import com.report.schoolparents.pojo.StudentProfileStatus;
import com.report.schoolparents.progress.ProgressFragment;
import com.report.schoolparents.webservice.RetrofitHelper;
import com.report.schoolparents.webservice.WebInterfaceimpl;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class MessageFragment extends Fragment {


    public MessageFragment() {
        // Required empty public constructor
    }

    View view;

    TextView txtNodata,txtStudentSelection;

    RecyclerView recyclerview;



    ProgressFragment progressFragment;

    ImageView imgselectstudent;

    String studentid="";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_add_message, container, false);

        txtNodata=view.findViewById(R.id.txtNodata);
        recyclerview=view.findViewById(R.id.recyclerview);
        txtStudentSelection=view.findViewById(R.id.txtStudentSelection);
        imgselectstudent=view.findViewById(R.id.imgselectstudent);

//        txtStudentSelection.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                getStudentList();
//
//            }
//        });
//
//        imgselectstudent.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                getStudentList();
//            }
//        });

        if(Constants.studentprofile!=null)
        {
            txtStudentSelection.setText("Selected student : "+Constants.studentprofile.getUdcStudentName());

            studentid=Constants.studentprofile.getUdcStudentId();

            getLeaveList(studentid);
        }

        return view;
    }


    public void getLeaveList(String studentid) {

        progressFragment = new ProgressFragment();
        progressFragment.show(getFragmentManager(), "cjkk");

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(getActivity());

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);
        Call<MessageStatus>listCall=webInterfaceimpl.getAllMessages(studentid);
        listCall.enqueue(new Callback<MessageStatus>() {
            @Override
            public void onResponse(Call<MessageStatus> call, Response<MessageStatus> response) {
                progressFragment.dismiss();

                if(response.body()!=null) {

                    if(response.body().getData().size()>0) {

                        recyclerview.setVisibility(View.VISIBLE);
                        txtNodata.setVisibility(View.GONE);

                        recyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
                        recyclerview.setAdapter(new MessageAdapter(getActivity(),response.body().getData()));





                    }
                    else {

                        Toast.makeText(getActivity(),"No data found",Toast.LENGTH_SHORT).show();
                        recyclerview.setVisibility(View.GONE);
                        txtNodata.setVisibility(View.VISIBLE);
                    }

                }

                else {

                    Toast.makeText(getActivity(),"No data found",Toast.LENGTH_SHORT).show();
                    recyclerview.setVisibility(View.GONE);
                    txtNodata.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onFailure(Call<MessageStatus> call, Throwable t) {
                progressFragment.dismiss();


                if(!NetConnection.isConnected(getActivity()))
                {
                    Toast.makeText(getActivity(),"Check Internet connection",Toast.LENGTH_SHORT).show();
                }
            }
        });


    }


    public void getStudentList()
    {

        progressFragment=new ProgressFragment();
        progressFragment.show(getChildFragmentManager(),"cjkk");

        Retrofit retrofitHelper= RetrofitHelper.getRetrofitInstance(getActivity());

        WebInterfaceimpl webInterfaceimpl=retrofitHelper.create(WebInterfaceimpl.class);

        Call<StudentProfileStatus> listCall=webInterfaceimpl.getStudentProfile();
        listCall.enqueue(new Callback<StudentProfileStatus>() {
            @Override
            public void onResponse(Call<StudentProfileStatus> call, Response<StudentProfileStatus> response) {

                progressFragment.dismiss();

                if(response.body()!=null) {

                    if(response.body().getData().size()>0) {

                        showStudentList(response.body().getData());
                    }
                    else {

                        Toast.makeText(getActivity(),"No data found",Toast.LENGTH_SHORT).show();

                    }

                }

                else {

                    Toast.makeText(getActivity(),"No data found",Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<StudentProfileStatus> call, Throwable t) {

                progressFragment.dismiss();


                if(!NetConnection.isConnected(getActivity()))
                {
                    Toast.makeText(getActivity(),"Check Internet connection",Toast.LENGTH_SHORT).show();
                }


            }
        });


    }


    public void showStudentList(final List<StudentProfile>students)
    {

        List<String>strings=new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose a student");
        ;

        for (StudentProfile student:students
        ) {

            strings.add(student.getUdcStudentName());

        }



        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtStudentSelection.setText(students.get(which).getUdcStudentName());

                studentid=students.get(which).getUdcStudentId();

                getLeaveList(studentid);

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

}
