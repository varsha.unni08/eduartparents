package com.report.schoolparents.fragments;


import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.report.schoolparents.AppLiterals.Constants;
import com.report.schoolparents.R;
import com.report.schoolparents.activities.MarkListActivity;
import com.report.schoolparents.connectivity.NetConnection;
import com.report.schoolparents.pojo.Student;
import com.report.schoolparents.pojo.StudentProfile;
import com.report.schoolparents.pojo.StudentProfileStatus;
import com.report.schoolparents.progress.ProgressFragment;
import com.report.schoolparents.webservice.RetrofitHelper;
import com.report.schoolparents.webservice.WebInterfaceimpl;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class ComplaintFragment extends Fragment {

    View view;

    EditText edtName,edtMessage;

    TextView txtStudentSelection;
    ImageView imgselectstudent;

    Button btnSubmit;

    String studentid;

    ProgressFragment progressFragment;




    public ComplaintFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_complaint, container, false);

        edtName=view.findViewById(R.id.edtName);
        edtMessage=view.findViewById(R.id.edtMessage);

        txtStudentSelection=view.findViewById(R.id.txtStudentSelection);
        imgselectstudent=view.findViewById(R.id.imgselectstudent);
        btnSubmit=view.findViewById(R.id.btnSubmit);

        if(Constants.studentprofile!=null)
        {
            txtStudentSelection.setText("Selected student : "+Constants.studentprofile.getUdcStudentName());

            studentid=Constants.studentprofile.getUdcStudentId();
        }

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!edtName.getText().toString().equals(""))
                {

                    if(!studentid.equals(""))
                    {
                        if(!edtMessage.getText().toString().equals(""))
                        {

postComplaint();

                        }
                        else {
                            Toast.makeText(getActivity(),"Enter a message",Toast.LENGTH_SHORT).show();
                        }


                    }
                    else {
                        Toast.makeText(getActivity(),"Select a student",Toast.LENGTH_SHORT).show();
                    }



                }
                else {
                    Toast.makeText(getActivity(),"Enter name",Toast.LENGTH_SHORT).show();
                }

            }
        });

        txtStudentSelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getStudentList();

            }
        });

        imgselectstudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getStudentList();
            }
        });

        return view;
    }



    public void postComplaint()
    {
        progressFragment=new ProgressFragment();
        progressFragment.show(getFragmentManager(),"cjkk");

        Retrofit retrofitHelper= RetrofitHelper.getRetrofitInstance(getActivity());

        WebInterfaceimpl webInterfaceimpl=retrofitHelper.create(WebInterfaceimpl.class);

        Call<JsonArray>jsonObjectCall=webInterfaceimpl.uploadParentComplaints(studentid,edtName.getText().toString(),edtMessage.getText().toString());

        jsonObjectCall.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                progressFragment.dismiss();

                if(response.body()!=null) {

                    try{

                        JSONArray jsonArray=new JSONArray(response.body().toString());

                        for(int i=0;i<jsonArray.length();i++) {

                            JSONObject jsonObject=jsonArray.getJSONObject(i);

                            if (jsonObject.getInt("status") == 1) {
                                Toast.makeText(getActivity(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();


                                edtMessage.setText("");
                                edtName.setText("");
                                studentid="";

                                txtStudentSelection.setText("Select a student");
                            }

                        }


                    }catch (Exception e)
                    {

                    }



                }
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                progressFragment.dismiss();


                if(!NetConnection.isConnected(getActivity()))
                {
                    Toast.makeText(getActivity(),"Check Internet connection",Toast.LENGTH_SHORT).show();
                }

            }
        });
    }









    public void getStudentList()
    {

        progressFragment=new ProgressFragment();
        progressFragment.show(getFragmentManager(),"cjkk");

        Retrofit retrofitHelper= RetrofitHelper.getRetrofitInstance(getActivity());

        WebInterfaceimpl webInterfaceimpl=retrofitHelper.create(WebInterfaceimpl.class);

        Call<StudentProfileStatus> listCall=webInterfaceimpl.getStudentProfile();
        listCall.enqueue(new Callback<StudentProfileStatus>() {
            @Override
            public void onResponse(Call<StudentProfileStatus> call, Response<StudentProfileStatus> response) {

                progressFragment.dismiss();

                if(response.body()!=null) {

                    if(response.body().getData().size()>0) {

                        showStudentList(response.body().getData());
                    }
                    else {

                        Toast.makeText(getActivity(),"No data found",Toast.LENGTH_SHORT).show();

                    }

                }

                else {

                    Toast.makeText(getActivity(),"No data found",Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<StudentProfileStatus> call, Throwable t) {

                progressFragment.dismiss();


                if(!NetConnection.isConnected(getActivity()))
                {
                    Toast.makeText(getActivity(),"Check Internet connection",Toast.LENGTH_SHORT).show();
                }


            }
        });


    }


    public void showStudentList(final List<StudentProfile>students)
    {

        List<String>strings=new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose a student");
        ;

        for (StudentProfile student:students
        ) {

            strings.add(student.getUdcStudentName());

        }



        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtStudentSelection.setText(students.get(which).getUdcStudentName());

                studentid=students.get(which).getUdcStudentId();

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

}
