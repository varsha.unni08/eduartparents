package com.report.schoolparents.webservice;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitHelperLogin {

    private static Retrofit retrofit;




    private static final String BASE_URL = "http://www.centroidsolutions.in/6_school/school_api/parent_api/";

    public static Retrofit getRetrofitInstance() {


        if (retrofit == null) {

            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit;
    }
}
