package com.report.schoolparents.webservice;



import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.report.schoolparents.pojo.Attendance;
import com.report.schoolparents.pojo.AttendanceStatus;
import com.report.schoolparents.pojo.Circular;
import com.report.schoolparents.pojo.CircularStatus;
import com.report.schoolparents.pojo.EntertainmentStatus;
import com.report.schoolparents.pojo.Entertainments;
import com.report.schoolparents.pojo.Events;
import com.report.schoolparents.pojo.Leave;
import com.report.schoolparents.pojo.LeaveStatus;
import com.report.schoolparents.pojo.LessonPlan;
import com.report.schoolparents.pojo.LessonPlanStatus;
import com.report.schoolparents.pojo.LoginToken;
import com.report.schoolparents.pojo.MarkList;
import com.report.schoolparents.pojo.Message;
import com.report.schoolparents.pojo.MessageStatus;
import com.report.schoolparents.pojo.OnlineClassStatus;
import com.report.schoolparents.pojo.ParentProfile;
import com.report.schoolparents.pojo.Student;
import com.report.schoolparents.pojo.StudentProfile;
import com.report.schoolparents.pojo.StudentProfileStatus;
import com.report.schoolparents.pojo.TeacherProfileStatus;
import com.report.schoolparents.pojo.TimeTable;
import com.report.schoolparents.pojo.TimeTableStatus;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface WebInterfaceimpl {

    @FormUrlEncoded
    @POST("Login.php")
    Call<LoginToken> getLoginData(@Field("user") String user, @Field("password") String password);


    @GET("get_student.php")
    Call<List<Student>>getStudentList();


    @GET("view_marklist_parent.php")
    Call<List<MarkList>>getMarkList(@Query("student_id") String student_id,@Query("exam_type")String exam_type,@Query("term")String term );


    @FormUrlEncoded
    @POST("parent_complaints.php")
    Call<JsonArray>uploadParentComplaints(@Field("student_id")String student_id,@Field("name")String name,@Field("messge")String messge);



    @GET("view_lessonplan_parent.php")
    Call<LessonPlanStatus>getAllLessonplans(@Query("student_id")String student_id);



    @GET("viewAllCirculars.php")
    Call<CircularStatus>getAllCirculars();

    @GET("view_leave_parent.php")
    Call<LeaveStatus>getAllLeaves(@Query("student_id")String student_id, @Query("month")String month, @Query("year")String year);


    @GET("parent_profile.php")
    Call<List<ParentProfile>>getParentProfile();

    @GET("view_timetable_parent.php")
    Call<TimeTableStatus>getTimetable(@Query("studentid")String student_id);

    @GET("student_profile.php")
    Call<StudentProfileStatus>getStudentProfile();

    @GET("getTeacherMobile.php")
    Call<TeacherProfileStatus>getTeacherMobile(@Query("studentid")String student_id);



    @FormUrlEncoded
    @POST("parent_leave_application.php")
    Call<JsonArray>applyParentLeave(@Field("student_id")String student_id,@Field("name")String name,@Field("from_date")String from_date,@Field("to_date")String to_date,@Field("reason")String reason);

    @GET("view_events_parent.php")
    Call<List<Events>>getEvents(@Query("month")String month,@Query("year")String year);


    @GET("view_attendance_parent.php")
    Call<List<Attendance>>getAttendance(@Query("student_id")String student_id,@Query("month")String month,@Query("year")String year);



    @FormUrlEncoded
    @POST("addEnterTainment.php")
    Call<JsonObject> addEntertainment(@Field("studentid")String student_id,@Field("name")String name,@Field("description")String description,
                                      @Field("photolink")String photolink,@Field("videolink")String videolink
      );

    @GET("view_parent_entertainment.php")
    Call<EntertainmentStatus>getAllEntertainments(@Query("studentid")String student_id);


    @GET("view_leave_parent.php")
    Call<AttendanceStatus>getStudentLeaveByParent(@Query("student_id")String student_id, @Query("month")String month, @Query("year")String year);



    @GET("viewAllMessages.php")
    Call<MessageStatus>getAllMessages(@Query("studentid")String student_id);

    @GET("viewOnlineClass.php")
    Call<OnlineClassStatus>viewOnlineClass(@Query("studentid")String student_id);




//
//
//    @Multipart
//    @POST("reporter.php?apicall=signup")
//    Call<List<RegisterData>> getRegistration(@Part("ds_reporter_user") RequestBody requestBody, @Part("ds_pincode") RequestBody ds_pincode, @Part("ds_mobile") RequestBody ds_mobile
//            , @Part("ds_education_qual") RequestBody ds_education_qual, @Part MultipartBody.Part multipartBody, @Part("ds_aadhaar") RequestBody ds_aadhaar
//            , @Part MultipartBody.Part adhar, @Part("ds_email") RequestBody ds_email, @Part("dt_birth") RequestBody dt_birth, @Part("ds_gender") RequestBody ds_gender, @Part("password") RequestBody password, @Part MultipartBody.Part ds_employee_image, @Part MultipartBody.Part police_verification
//            , @Part("ds_transaction") RequestBody ds_transaction);
//
//
//
//    @GET("reporterOtpGenerator.php")
//    Call<List<OtpGenerateData>>generateOTp();
//
//    @FormUrlEncoded
//    @POST("reporterOtpVerification.php")
//    Call<List<OtpGenerateData>>otpVerification(@Field("otp") String otp);
//
//    @GET("reporterProfile.php")
//    Call<List<ReporterProfile>>getReporterProfile();
//
//    @Multipart
//    @POST("reporterEditProfileImage.php")
//    Call<List<RegisterData>>EditProfileimg(@Part MultipartBody.Part ds_employee_image);
//
//    @GET("stateDistPin_reporter.php?apicall=state")
//    Call<List<State>>getState();
//
//    @GET("stateDistPin_reporter.php?apicall=district")
//    Call<List<District>>getDistrict(@Query("state") String state);
//
//    @GET("stateDistPin_reporter.php?apicall=pincode")
//    Call<List<PinCode>>getPincode(@Query("district") String district);
//
//    @GET("stateDistPin_reporter.php?apicall=village")
//    Call<List<Village>>getVillage(@Query("pincode") String pincode);
//
//
//    @POST("reporter_newses.php")
//    Call<List<NewsReport>>getReporterNews();
//
//    @POST("reporter_news_categories.php")
//    Call<List<NewsCategory>>getNewsCategory();
//
//    @FormUrlEncoded
//    @POST("search_news_bydate.php")
//    Call<List<NewsReport>>getSearchNewsByDate(@Field("dt_record") String dt_record);
//
//
//    @Multipart
//    @POST("news.php")
//    Call<List<NewsPreviewResponse>>postNews(@Part("ds_news_head") RequestBody ds_news_head, @Part("ds_news_content") RequestBody ds_news_content,
//                                            @Part("ds_news_priority") RequestBody ds_news_priority, @Part("cd_news_category") RequestBody cd_news_category,
//                                            @Part MultipartBody.Part ds_image, @Part MultipartBody.Part ds_video);
//
//
//
//    @FormUrlEncoded
//    @POST("reporter_details.php")
//    Call<List<Reporter>>getReporterdetail(@Field("pincode") String pincode);



















//    @GET("adm_patterns.php")
//    Call<List<Pattern>>getPatters();
//
//    @GET("adm_product_stock.php")
//    Call<List<StockData>>getProductStock(@Query("cd_pattern") String cd_pattern);
//
//    @GET("adm_product_stock.php")
//    Call<List<Branchdata>>getBranchData(@Query("cd_product") String cd_product);
}
