package com.report.schoolparents.pojo;

public class Period {
    String day;
    int selected=0;

    public Period() {
    }

    public int getSelected() {
        return selected;
    }

    public void setSelected(int selected) {
        this.selected = selected;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }
}
