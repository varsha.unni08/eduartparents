package com.report.schoolparents.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TeacherProfile {


    @SerializedName("udc_teacher_id")
    @Expose
    private String udcTeacherId;
    @SerializedName("udc_teacher_name")
    @Expose
    private String udcTeacherName;
    @SerializedName("udc_teacherLastName")
    @Expose
    private String udcTeacherLastName;
    @SerializedName("udc_teacher_permanent_address")
    @Expose
    private String udcTeacherPermanentAddress;
    @SerializedName("udc_teacher_temporary_address")
    @Expose
    private String udcTeacherTemporaryAddress;
    @SerializedName("udc_teacher_code")
    @Expose
    private String udcTeacherCode;
    @SerializedName("udc_teacher_mob")
    @Expose
    private String udcTeacherMob;
    @SerializedName("udc_teacher_aadar_no")
    @Expose
    private String udcTeacherAadarNo;
    @SerializedName("udc_teacher_aadar_image_path")
    @Expose
    private String udcTeacherAadarImagePath;
    @SerializedName("udc_teacher_Image")
    @Expose
    private String udcTeacherImage;
    @SerializedName("udc_teacher_qualification")
    @Expose
    private String udcTeacherQualification;
    @SerializedName("udc_teacher_status")
    @Expose
    private String udcTeacherStatus;
    @SerializedName("udc_teacher_email_id")
    @Expose
    private String udcTeacherEmailId;
    @SerializedName("udc_teacher_dob")
    @Expose
    private String udcTeacherDob;
    @SerializedName("udc_teacher_joint_date")
    @Expose
    private String udcTeacherJointDate;
    @SerializedName("udc_teacher_service_start_date")
    @Expose
    private String udcTeacherServiceStartDate;
    @SerializedName("udc_teacher_previous_schools_details")
    @Expose
    private String udcTeacherPreviousSchoolsDetails;
    @SerializedName("udc_teacher_password")
    @Expose
    private String udcTeacherPassword;
    @SerializedName("udc_employee_status")
    @Expose
    private String udcEmployeeStatus;
    @SerializedName("udc_teacher_leaving_date")
    @Expose
    private String udcTeacherLeavingDate;
    @SerializedName("udc_teacher_deactivate_date")
    @Expose
    private String udcTeacherDeactivateDate;

    public TeacherProfile() {
    }

    public String getUdcTeacherId() {
        return udcTeacherId;
    }

    public void setUdcTeacherId(String udcTeacherId) {
        this.udcTeacherId = udcTeacherId;
    }

    public String getUdcTeacherName() {
        return udcTeacherName;
    }

    public void setUdcTeacherName(String udcTeacherName) {
        this.udcTeacherName = udcTeacherName;
    }

    public String getUdcTeacherLastName() {
        return udcTeacherLastName;
    }

    public void setUdcTeacherLastName(String udcTeacherLastName) {
        this.udcTeacherLastName = udcTeacherLastName;
    }

    public String getUdcTeacherPermanentAddress() {
        return udcTeacherPermanentAddress;
    }

    public void setUdcTeacherPermanentAddress(String udcTeacherPermanentAddress) {
        this.udcTeacherPermanentAddress = udcTeacherPermanentAddress;
    }

    public String getUdcTeacherTemporaryAddress() {
        return udcTeacherTemporaryAddress;
    }

    public void setUdcTeacherTemporaryAddress(String udcTeacherTemporaryAddress) {
        this.udcTeacherTemporaryAddress = udcTeacherTemporaryAddress;
    }

    public String getUdcTeacherCode() {
        return udcTeacherCode;
    }

    public void setUdcTeacherCode(String udcTeacherCode) {
        this.udcTeacherCode = udcTeacherCode;
    }

    public String getUdcTeacherMob() {
        return udcTeacherMob;
    }

    public void setUdcTeacherMob(String udcTeacherMob) {
        this.udcTeacherMob = udcTeacherMob;
    }

    public String getUdcTeacherAadarNo() {
        return udcTeacherAadarNo;
    }

    public void setUdcTeacherAadarNo(String udcTeacherAadarNo) {
        this.udcTeacherAadarNo = udcTeacherAadarNo;
    }

    public String getUdcTeacherAadarImagePath() {
        return udcTeacherAadarImagePath;
    }

    public void setUdcTeacherAadarImagePath(String udcTeacherAadarImagePath) {
        this.udcTeacherAadarImagePath = udcTeacherAadarImagePath;
    }

    public String getUdcTeacherImage() {
        return udcTeacherImage;
    }

    public void setUdcTeacherImage(String udcTeacherImage) {
        this.udcTeacherImage = udcTeacherImage;
    }

    public String getUdcTeacherQualification() {
        return udcTeacherQualification;
    }

    public void setUdcTeacherQualification(String udcTeacherQualification) {
        this.udcTeacherQualification = udcTeacherQualification;
    }

    public String getUdcTeacherStatus() {
        return udcTeacherStatus;
    }

    public void setUdcTeacherStatus(String udcTeacherStatus) {
        this.udcTeacherStatus = udcTeacherStatus;
    }

    public String getUdcTeacherEmailId() {
        return udcTeacherEmailId;
    }

    public void setUdcTeacherEmailId(String udcTeacherEmailId) {
        this.udcTeacherEmailId = udcTeacherEmailId;
    }

    public String getUdcTeacherDob() {
        return udcTeacherDob;
    }

    public void setUdcTeacherDob(String udcTeacherDob) {
        this.udcTeacherDob = udcTeacherDob;
    }

    public String getUdcTeacherJointDate() {
        return udcTeacherJointDate;
    }

    public void setUdcTeacherJointDate(String udcTeacherJointDate) {
        this.udcTeacherJointDate = udcTeacherJointDate;
    }

    public String getUdcTeacherServiceStartDate() {
        return udcTeacherServiceStartDate;
    }

    public void setUdcTeacherServiceStartDate(String udcTeacherServiceStartDate) {
        this.udcTeacherServiceStartDate = udcTeacherServiceStartDate;
    }

    public String getUdcTeacherPreviousSchoolsDetails() {
        return udcTeacherPreviousSchoolsDetails;
    }

    public void setUdcTeacherPreviousSchoolsDetails(String udcTeacherPreviousSchoolsDetails) {
        this.udcTeacherPreviousSchoolsDetails = udcTeacherPreviousSchoolsDetails;
    }

    public String getUdcTeacherPassword() {
        return udcTeacherPassword;
    }

    public void setUdcTeacherPassword(String udcTeacherPassword) {
        this.udcTeacherPassword = udcTeacherPassword;
    }

    public String getUdcEmployeeStatus() {
        return udcEmployeeStatus;
    }

    public void setUdcEmployeeStatus(String udcEmployeeStatus) {
        this.udcEmployeeStatus = udcEmployeeStatus;
    }

    public String getUdcTeacherLeavingDate() {
        return udcTeacherLeavingDate;
    }

    public void setUdcTeacherLeavingDate(String udcTeacherLeavingDate) {
        this.udcTeacherLeavingDate = udcTeacherLeavingDate;
    }

    public String getUdcTeacherDeactivateDate() {
        return udcTeacherDeactivateDate;
    }

    public void setUdcTeacherDeactivateDate(String udcTeacherDeactivateDate) {
        this.udcTeacherDeactivateDate = udcTeacherDeactivateDate;
    }
}
