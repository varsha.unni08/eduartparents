package com.report.schoolparents.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ParentProfile {

    @SerializedName("udc_parent")
    @Expose
    private String udcParent;
    @SerializedName("udc_father_name")
    @Expose
    private String udcFatherName;
    @SerializedName("udc_father_contact_address")
    @Expose
    private String udcFatherContactAddress;
    @SerializedName("udc_father_contact_mobile")
    @Expose
    private String udcFatherContactMobile;
    @SerializedName("udc_father_occupation")
    @Expose
    private String udcFatherOccupation;
    @SerializedName("udc_father_qualification")
    @Expose
    private String udcFatherQualification;
    @SerializedName("udc_father_email_id")
    @Expose
    private String udcFatherEmailId;

    public ParentProfile() {
    }

    public String getUdcParent() {
        return udcParent;
    }

    public void setUdcParent(String udcParent) {
        this.udcParent = udcParent;
    }

    public String getUdcFatherName() {
        return udcFatherName;
    }

    public void setUdcFatherName(String udcFatherName) {
        this.udcFatherName = udcFatherName;
    }

    public String getUdcFatherContactAddress() {
        return udcFatherContactAddress;
    }

    public void setUdcFatherContactAddress(String udcFatherContactAddress) {
        this.udcFatherContactAddress = udcFatherContactAddress;
    }

    public String getUdcFatherContactMobile() {
        return udcFatherContactMobile;
    }

    public void setUdcFatherContactMobile(String udcFatherContactMobile) {
        this.udcFatherContactMobile = udcFatherContactMobile;
    }

    public String getUdcFatherOccupation() {
        return udcFatherOccupation;
    }

    public void setUdcFatherOccupation(String udcFatherOccupation) {
        this.udcFatherOccupation = udcFatherOccupation;
    }

    public String getUdcFatherQualification() {
        return udcFatherQualification;
    }

    public void setUdcFatherQualification(String udcFatherQualification) {
        this.udcFatherQualification = udcFatherQualification;
    }

    public String getUdcFatherEmailId() {
        return udcFatherEmailId;
    }

    public void setUdcFatherEmailId(String udcFatherEmailId) {
        this.udcFatherEmailId = udcFatherEmailId;
    }
}
