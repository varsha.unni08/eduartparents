package com.report.schoolparents.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OnlineClass{


    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("classtopic")
    @Expose
    private String classtopic;
    @SerializedName("classdivision")
    @Expose
    private String classdivision;
    @SerializedName("class")
    @Expose
    private String _class;
    @SerializedName("joinlink")
    @Expose
    private String joinlink;
    @SerializedName("time")
    @Expose
    private String time;

    public OnlineClass() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getClasstopic() {
        return classtopic;
    }

    public void setClasstopic(String classtopic) {
        this.classtopic = classtopic;
    }

    public String getClassdivision() {
        return classdivision;
    }

    public void setClassdivision(String classdivision) {
        this.classdivision = classdivision;
    }

    public String get_class() {
        return _class;
    }

    public void set_class(String _class) {
        this._class = _class;
    }

    public String getJoinlink() {
        return joinlink;
    }

    public void setJoinlink(String joinlink) {
        this.joinlink = joinlink;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
