package com.report.schoolparents.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LessonPlan {

    @SerializedName("student_id")
    @Expose
    private String studentId;
    @SerializedName("udc_subject")
    @Expose
    private String udcSubject;
    @SerializedName("udc_lessonplan_topic")
    @Expose
    private String udcLessonplanTopic;
    @SerializedName("udc_lessonplan_description")
    @Expose
    private String udcLessonplanDescription;
    @SerializedName("udc_teacher_name")
    @Expose
    private String udcTeacherName;

    public LessonPlan() {
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getUdcSubject() {
        return udcSubject;
    }

    public void setUdcSubject(String udcSubject) {
        this.udcSubject = udcSubject;
    }

    public String getUdcLessonplanTopic() {
        return udcLessonplanTopic;
    }

    public void setUdcLessonplanTopic(String udcLessonplanTopic) {
        this.udcLessonplanTopic = udcLessonplanTopic;
    }

    public String getUdcLessonplanDescription() {
        return udcLessonplanDescription;
    }

    public void setUdcLessonplanDescription(String udcLessonplanDescription) {
        this.udcLessonplanDescription = udcLessonplanDescription;
    }

    public String getUdcTeacherName() {
        return udcTeacherName;
    }

    public void setUdcTeacherName(String udcTeacherName) {
        this.udcTeacherName = udcTeacherName;
    }
}
