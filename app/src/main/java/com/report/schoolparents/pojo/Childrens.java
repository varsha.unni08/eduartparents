package com.report.schoolparents.pojo;

public class Childrens {

    String name;
    String url;

    public Childrens() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
