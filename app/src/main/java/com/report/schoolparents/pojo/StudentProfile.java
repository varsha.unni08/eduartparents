package com.report.schoolparents.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class StudentProfile  implements Serializable {

    @SerializedName("udc_student_id")
    @Expose
    private String udcStudentId;
    @SerializedName("udc_student_admission_no")
    @Expose
    private String udcStudentAdmissionNo;
    @SerializedName("udc_student_name")
    @Expose
    private String udcStudentName;
    @SerializedName("udc_student_last_name")
    @Expose
    private String udcStudentLastName;
    @SerializedName("udc_student_religion")
    @Expose
    private String udcStudentReligion;
    @SerializedName("udc_student_cast_in_category")
    @Expose
    private String udcStudentCastInCategory;
    @SerializedName("udc_student_cast")
    @Expose
    private String udcStudentCast;
    @SerializedName("udc_student_stipent_eligibility")
    @Expose
    private String udcStudentStipentEligibility;
    @SerializedName("udc_student_dob")
    @Expose
    private String udcStudentDob;
    @SerializedName("udc_student_identification_marks")
    @Expose
    private String udcStudentIdentificationMarks;
    @SerializedName("udc_student_photo_path")
    @Expose
    private String udcStudentPhotoPath;
    @SerializedName("udc_student_aadar_no")
    @Expose
    private String udcStudentAadarNo;
    @SerializedName("udc_student_aadar_image_path")
    @Expose
    private String udcStudentAadarImagePath;
    @SerializedName("udc_student_join_date")
    @Expose
    private String udcStudentJoinDate;
    @SerializedName("udc_student_leaving_date")
    @Expose
    private String udcStudentLeavingDate;
    @SerializedName("udc_student_status")
    @Expose
    private String udcStudentStatus;
    @SerializedName("udc_student_leaving_reason")
    @Expose
    private String udcStudentLeavingReason;
    @SerializedName("udc_student_classof_addmission_first")
    @Expose
    private String udcStudentClassofAddmissionFirst;
    @SerializedName("udc_student_previous_school")
    @Expose
    private String udcStudentPreviousSchool;
    @SerializedName("udc_student_password")
    @Expose
    private String udcStudentPassword;
    @SerializedName("udc_parent_id")
    @Expose
    private String udcParentId;
    public StudentProfile() {
    }

    public String getUdcStudentId() {
        return udcStudentId;
    }

    public void setUdcStudentId(String udcStudentId) {
        this.udcStudentId = udcStudentId;
    }

    public String getUdcStudentAdmissionNo() {
        return udcStudentAdmissionNo;
    }

    public void setUdcStudentAdmissionNo(String udcStudentAdmissionNo) {
        this.udcStudentAdmissionNo = udcStudentAdmissionNo;
    }

    public String getUdcStudentName() {
        return udcStudentName;
    }

    public void setUdcStudentName(String udcStudentName) {
        this.udcStudentName = udcStudentName;
    }

    public String getUdcStudentLastName() {
        return udcStudentLastName;
    }

    public void setUdcStudentLastName(String udcStudentLastName) {
        this.udcStudentLastName = udcStudentLastName;
    }

    public String getUdcStudentReligion() {
        return udcStudentReligion;
    }

    public void setUdcStudentReligion(String udcStudentReligion) {
        this.udcStudentReligion = udcStudentReligion;
    }

    public String getUdcStudentCastInCategory() {
        return udcStudentCastInCategory;
    }

    public void setUdcStudentCastInCategory(String udcStudentCastInCategory) {
        this.udcStudentCastInCategory = udcStudentCastInCategory;
    }

    public String getUdcStudentCast() {
        return udcStudentCast;
    }

    public void setUdcStudentCast(String udcStudentCast) {
        this.udcStudentCast = udcStudentCast;
    }

    public String getUdcStudentStipentEligibility() {
        return udcStudentStipentEligibility;
    }

    public void setUdcStudentStipentEligibility(String udcStudentStipentEligibility) {
        this.udcStudentStipentEligibility = udcStudentStipentEligibility;
    }

    public String getUdcStudentDob() {
        return udcStudentDob;
    }

    public void setUdcStudentDob(String udcStudentDob) {
        this.udcStudentDob = udcStudentDob;
    }

    public String getUdcStudentIdentificationMarks() {
        return udcStudentIdentificationMarks;
    }

    public void setUdcStudentIdentificationMarks(String udcStudentIdentificationMarks) {
        this.udcStudentIdentificationMarks = udcStudentIdentificationMarks;
    }

    public String getUdcStudentPhotoPath() {
        return udcStudentPhotoPath;
    }

    public void setUdcStudentPhotoPath(String udcStudentPhotoPath) {
        this.udcStudentPhotoPath = udcStudentPhotoPath;
    }

    public String getUdcStudentAadarNo() {
        return udcStudentAadarNo;
    }

    public void setUdcStudentAadarNo(String udcStudentAadarNo) {
        this.udcStudentAadarNo = udcStudentAadarNo;
    }

    public String getUdcStudentAadarImagePath() {
        return udcStudentAadarImagePath;
    }

    public void setUdcStudentAadarImagePath(String udcStudentAadarImagePath) {
        this.udcStudentAadarImagePath = udcStudentAadarImagePath;
    }

    public String getUdcStudentJoinDate() {
        return udcStudentJoinDate;
    }

    public void setUdcStudentJoinDate(String udcStudentJoinDate) {
        this.udcStudentJoinDate = udcStudentJoinDate;
    }

    public String getUdcStudentLeavingDate() {
        return udcStudentLeavingDate;
    }

    public void setUdcStudentLeavingDate(String udcStudentLeavingDate) {
        this.udcStudentLeavingDate = udcStudentLeavingDate;
    }

    public String getUdcStudentStatus() {
        return udcStudentStatus;
    }

    public void setUdcStudentStatus(String udcStudentStatus) {
        this.udcStudentStatus = udcStudentStatus;
    }

    public String getUdcStudentLeavingReason() {
        return udcStudentLeavingReason;
    }

    public void setUdcStudentLeavingReason(String udcStudentLeavingReason) {
        this.udcStudentLeavingReason = udcStudentLeavingReason;
    }

    public String getUdcStudentClassofAddmissionFirst() {
        return udcStudentClassofAddmissionFirst;
    }

    public void setUdcStudentClassofAddmissionFirst(String udcStudentClassofAddmissionFirst) {
        this.udcStudentClassofAddmissionFirst = udcStudentClassofAddmissionFirst;
    }

    public String getUdcStudentPreviousSchool() {
        return udcStudentPreviousSchool;
    }

    public void setUdcStudentPreviousSchool(String udcStudentPreviousSchool) {
        this.udcStudentPreviousSchool = udcStudentPreviousSchool;
    }

    public String getUdcStudentPassword() {
        return udcStudentPassword;
    }

    public void setUdcStudentPassword(String udcStudentPassword) {
        this.udcStudentPassword = udcStudentPassword;
    }

    public String getUdcParentId() {
        return udcParentId;
    }

    public void setUdcParentId(String udcParentId) {
        this.udcParentId = udcParentId;
    }
}
