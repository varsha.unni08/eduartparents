package com.report.schoolparents.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Student {


    @SerializedName("udc_student_id")
    @Expose
    private String udcStudentId;
    @SerializedName("udc_student_name")
    @Expose
    private String udcStudentName;


    public Student() {
    }

    public String getUdcStudentId() {
        return udcStudentId;
    }

    public void setUdcStudentId(String udcStudentId) {
        this.udcStudentId = udcStudentId;
    }

    public String getUdcStudentName() {
        return udcStudentName;
    }

    public void setUdcStudentName(String udcStudentName) {
        this.udcStudentName = udcStudentName;
    }
}
