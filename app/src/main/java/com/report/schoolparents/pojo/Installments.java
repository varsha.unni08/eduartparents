package com.report.schoolparents.pojo;

public class Installments {

    String Feename;
    String fees;
    int status;

    public Installments() {
    }


    public String getFeename() {
        return Feename;
    }

    public void setFeename(String feename) {
        Feename = feename;
    }

    public String getFees() {
        return fees;
    }

    public void setFees(String fees) {
        this.fees = fees;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
