package com.report.schoolparents.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MarkList {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("subject")
    @Expose
    private String subject;
    @SerializedName("marks_scored")
    @Expose
    private String marksScored;
    @SerializedName("total_marks")
    @Expose
    private String totalMarks;
    @SerializedName("treview")
    @Expose
    private String treview;
    @SerializedName("hmreview")
    @Expose
    private String hmreview;
    @SerializedName("Parentreview")
    @Expose
    private String parentreview;


    public MarkList() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMarksScored() {
        return marksScored;
    }

    public void setMarksScored(String marksScored) {
        this.marksScored = marksScored;
    }

    public String getTotalMarks() {
        return totalMarks;
    }

    public void setTotalMarks(String totalMarks) {
        this.totalMarks = totalMarks;
    }

    public String getTreview() {
        return treview;
    }

    public void setTreview(String treview) {
        this.treview = treview;
    }

    public String getHmreview() {
        return hmreview;
    }

    public void setHmreview(String hmreview) {
        this.hmreview = hmreview;
    }

    public String getParentreview() {
        return parentreview;
    }

    public void setParentreview(String parentreview) {
        this.parentreview = parentreview;
    }


}
