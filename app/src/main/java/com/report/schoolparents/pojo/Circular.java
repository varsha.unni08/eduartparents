package com.report.schoolparents.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Circular {

    @SerializedName("udc_circular_id")
    @Expose
    private String udcCircularId;
    @SerializedName("udc_circular_head")
    @Expose
    private String udcCircularHead;
    @SerializedName("udc_circular_content")
    @Expose
    private String udcCircularContent;
    @SerializedName("udc_date_prep")
    @Expose
    private String udcDatePrep;
    @SerializedName("udc_circular_send_status")
    @Expose
    private String udcCircularSendStatus;
    @SerializedName("udc_circular_recipient")
    @Expose
    private String udcCircularRecipient;
    @SerializedName("udc_accademic_year_id")
    @Expose
    private String udcAccademicYearId;
    @SerializedName("udc_parent_status")
    @Expose
    private String udcParentStatus;
    @SerializedName("udc_teacher_status")
    @Expose
    private String udcTeacherStatus;
    @SerializedName("udc_circular_deleteid")
    @Expose
    private String udcCircularDeleteid;

    public Circular() {
    }

    public String getUdcCircularId() {
        return udcCircularId;
    }

    public void setUdcCircularId(String udcCircularId) {
        this.udcCircularId = udcCircularId;
    }

    public String getUdcCircularHead() {
        return udcCircularHead;
    }

    public void setUdcCircularHead(String udcCircularHead) {
        this.udcCircularHead = udcCircularHead;
    }

    public String getUdcCircularContent() {
        return udcCircularContent;
    }

    public void setUdcCircularContent(String udcCircularContent) {
        this.udcCircularContent = udcCircularContent;
    }

    public String getUdcDatePrep() {
        return udcDatePrep;
    }

    public void setUdcDatePrep(String udcDatePrep) {
        this.udcDatePrep = udcDatePrep;
    }

    public String getUdcCircularSendStatus() {
        return udcCircularSendStatus;
    }

    public void setUdcCircularSendStatus(String udcCircularSendStatus) {
        this.udcCircularSendStatus = udcCircularSendStatus;
    }

    public String getUdcCircularRecipient() {
        return udcCircularRecipient;
    }

    public void setUdcCircularRecipient(String udcCircularRecipient) {
        this.udcCircularRecipient = udcCircularRecipient;
    }

    public String getUdcAccademicYearId() {
        return udcAccademicYearId;
    }

    public void setUdcAccademicYearId(String udcAccademicYearId) {
        this.udcAccademicYearId = udcAccademicYearId;
    }

    public String getUdcParentStatus() {
        return udcParentStatus;
    }

    public void setUdcParentStatus(String udcParentStatus) {
        this.udcParentStatus = udcParentStatus;
    }

    public String getUdcTeacherStatus() {
        return udcTeacherStatus;
    }

    public void setUdcTeacherStatus(String udcTeacherStatus) {
        this.udcTeacherStatus = udcTeacherStatus;
    }

    public String getUdcCircularDeleteid() {
        return udcCircularDeleteid;
    }

    public void setUdcCircularDeleteid(String udcCircularDeleteid) {
        this.udcCircularDeleteid = udcCircularDeleteid;
    }
}
