package com.report.schoolparents.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Message {

    @SerializedName("udc_message_id")
    @Expose
    private String udcMessageId;
    @SerializedName("udc_accademic_year_id")
    @Expose
    private String udcAccademicYearId;
    @SerializedName("udc_teacher_id")
    @Expose
    private String udcTeacherId;
    @SerializedName("udc_class")
    @Expose
    private String udcClass;
    @SerializedName("udc_division")
    @Expose
    private String udcDivision;
    @SerializedName("udc_message_date")
    @Expose
    private String udcMessageDate;
    @SerializedName("udc_message_topic")
    @Expose
    private String udcMessageTopic;
    @SerializedName("udc_message_description")
    @Expose
    private String udcMessageDescription;
    @SerializedName("delete_status")
    @Expose
    private String deleteStatus;
    @SerializedName("udc_teacher_name")
    @Expose
    private  String udc_teacher_name;

    public Message() {
    }

    public String getUdc_teacher_name() {
        return udc_teacher_name;
    }

    public void setUdc_teacher_name(String udc_teacher_name) {
        this.udc_teacher_name = udc_teacher_name;
    }

    public String getUdcMessageTopic() {
        return udcMessageTopic;
    }

    public void setUdcMessageTopic(String udcMessageTopic) {
        this.udcMessageTopic = udcMessageTopic;
    }

    public String getUdcMessageDescription() {
        return udcMessageDescription;
    }

    public void setUdcMessageDescription(String udcMessageDescription) {
        this.udcMessageDescription = udcMessageDescription;
    }

    public String getUdcMessageDate() {
        return udcMessageDate;
    }

    public void setUdcMessageDate(String udcMessageDate) {
        this.udcMessageDate = udcMessageDate;
    }


}
