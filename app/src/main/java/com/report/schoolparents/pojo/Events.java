package com.report.schoolparents.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Events {
    @SerializedName("udc_event_name")
    @Expose
    private String udcEventName="";
    @SerializedName("udc_event_description")
    @Expose
    private String udcEventDescription="";
    @SerializedName("udc_event_date")
    @Expose
    private String udc_event_date="";

    int index;
    String data="";
    String calendardate="";

    public String getCalendardate() {
        return calendardate;
    }

    public void setCalendardate(String calendardate) {
        this.calendardate = calendardate;
    }

    public String getUdc_event_date() {
        return udc_event_date;
    }

    public void setUdc_event_date(String udc_event_date) {
        this.udc_event_date = udc_event_date;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Events() {
    }

    public String getUdcEventName() {
        return udcEventName;
    }

    public void setUdcEventName(String udcEventName) {
        this.udcEventName = udcEventName;
    }

    public String getUdcEventDescription() {
        return udcEventDescription;
    }

    public void setUdcEventDescription(String udcEventDescription) {
        this.udcEventDescription = udcEventDescription;
    }
}
