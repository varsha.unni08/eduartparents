package com.report.schoolparents.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.report.schoolparents.AppLiterals.Constants;
import com.report.schoolparents.R;
import com.report.schoolparents.activities.media.CaptureImageActivity;
import com.report.schoolparents.activities.media.RecordVideoActivity;
import com.report.schoolparents.connectivity.NetConnection;
import com.report.schoolparents.pojo.Student;
import com.report.schoolparents.pojo.StudentProfile;
import com.report.schoolparents.pojo.StudentProfileStatus;
import com.report.schoolparents.progress.ProgressFragment;
import com.report.schoolparents.utils.Utilities;
import com.report.schoolparents.webservice.RetrofitHelper;
import com.report.schoolparents.webservice.RetrofitHelperLogin;
import com.report.schoolparents.webservice.WebInterfaceimpl;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class AddEntertainActivity extends AppCompatActivity {



    TextView txttapimage,txtStudentSelection;

    public static final int image=11,video=112;



    ImageView imgback;

    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS=111;


    String filepath="",videofilepath="";





    ImageView imgselectstudent;

    Button btnSubmit;

    String studentid="";

    ProgressFragment progressFragment;

    EditText edtName,edtDescription,edtPhotolink,edtVideolink;



    List<String> listPermissionsNeeded = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_entertain);
        getSupportActionBar().hide();


        imgselectstudent=findViewById(R.id.imgselectstudent);
        edtName=findViewById(R.id.edtName);
        edtDescription=findViewById(R.id.edtDescription);
        edtPhotolink=findViewById(R.id.edtPhotolink);
        edtVideolink=findViewById(R.id.edtVideolink);



        txttapimage=findViewById(R.id.txttapimage);


        txtStudentSelection=findViewById(R.id.txtStudentSelection);
        btnSubmit=findViewById(R.id.btnSubmit);


        imgback=findViewById(R.id.imgback);


        if(Constants.studentprofile!=null)
        {
            txtStudentSelection.setText("Student name : "+Constants.studentprofile.getUdcStudentName());
            studentid=Constants.studentprofile.getUdcStudentId();

        }



        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!studentid.equalsIgnoreCase(""))
                {

                    if(!edtName.getText().toString().equalsIgnoreCase(""))
                    {

                        if(!edtDescription.getText().toString().equalsIgnoreCase(""))
                        {

                            if(!edtPhotolink.getText().toString().equalsIgnoreCase("")||!edtVideolink.getText().toString().equalsIgnoreCase(""))
                            {

                                MultipartBody.Part body_image =null;
                                MultipartBody.Part body_video =null;

                                progressFragment = new ProgressFragment();
                                progressFragment.show(getSupportFragmentManager(), "dklk");
                                WebInterfaceimpl webInterfaceimpl = RetrofitHelper.getRetrofitInstance(AddEntertainActivity.this).create(WebInterfaceimpl.class);







                                Call<JsonObject>jsonArrayCall=webInterfaceimpl.addEntertainment(studentid,edtName.getText().toString(),edtDescription.getText().toString(),edtPhotolink.getText().toString(),edtVideolink.getText().toString());

                                jsonArrayCall.enqueue(new Callback<JsonObject>() {
                                    @Override
                                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                        progressFragment.dismiss();

                                        if(response.body()!=null)
                                        {

                                            try{

                                                JSONObject jsonObject=new JSONObject(response.body().toString());

                                                if(jsonObject.getInt("status")==1)
                                                {

                                                   Toast.makeText(AddEntertainActivity.this,jsonObject.getString("Success"),Toast.LENGTH_SHORT).show();
                                                }
                                                else {

                                                    Toast.makeText(AddEntertainActivity.this,jsonObject.getString("Failed"),Toast.LENGTH_SHORT).show();

                                                }




                                            }catch (Exception e)
                                            {

                                            }



                                        }

                                       // Toast.makeText(AddEntertainActivity.this,response.body().toString(),Toast.LENGTH_SHORT).show();






                                    }

                                    @Override
                                    public void onFailure(Call<JsonObject> call, Throwable t) {
                                        progressFragment.dismiss();


                                        if(!NetConnection.isConnected(AddEntertainActivity.this))
                                        {
                                            Toast.makeText(AddEntertainActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });



                            }
                            else {

                                Toast.makeText(AddEntertainActivity.this,"please add any program file",Toast.LENGTH_SHORT).show();
                            }


                        }
                        else {

                            Toast.makeText(AddEntertainActivity.this,"enter a description",Toast.LENGTH_SHORT).show();
                        }


                    }
                    else {

                        Toast.makeText(AddEntertainActivity.this,"enter a name",Toast.LENGTH_SHORT).show();
                    }


                }
                else {

                    Toast.makeText(AddEntertainActivity.this,"Select a student",Toast.LENGTH_SHORT).show();
                }


            }
        });



        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                onBackPressed();
            }
        });



//        imgselectstudent.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                getStudentList();
//
//            }
//        });
//
//        txtStudentSelection.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                getStudentList();
//
//            }
//        });


















    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == image&&data!=null&&resultCode==RESULT_OK) {

             filepath=data.getStringExtra("imagefile");



        }
        else if (requestCode == video&&data!=null&&resultCode==RESULT_OK) {

            videofilepath=data.getStringExtra("videofile");





        }


    }

    private boolean checkAndRequestPermissions() {
        int camera = ContextCompat.checkSelfPermission(AddEntertainActivity.this,
                Manifest.permission.CAMERA);
        int writepermission = ContextCompat.checkSelfPermission(AddEntertainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        int read = ContextCompat.checkSelfPermission(AddEntertainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE);


        int audio = ContextCompat.checkSelfPermission(AddEntertainActivity.this, Manifest.permission.RECORD_AUDIO);



        if (writepermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (camera != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (read != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (audio != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.RECORD_AUDIO);
        }

        if (!listPermissionsNeeded.isEmpty()) {
                        return false;
        }
        return true;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);


        if (requestCode == REQUEST_ID_MULTIPLE_PERMISSIONS) {


            if (grantResults.length > 0) {
                for (int i = 0; i < permissions.length; i++) {


                    if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                           // Log.e("msg", "accounts granted");

                        }
                    } else if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                           // Log.e("msg", "storage granted");

                        }
                    } else if (permissions[i].equals(Manifest.permission.CAMERA)) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                           // Log.e("msg", "call granted");

                        }
                    } else if (permissions[i].equals(Manifest.permission.RECORD_AUDIO)) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            // Log.e("msg", "call granted");

                        }
                    }

                }

            }

        }
    }



    public void getStudentList()
    {

        progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"cjkk");

        Retrofit retrofitHelper= RetrofitHelper.getRetrofitInstance(AddEntertainActivity.this);

        WebInterfaceimpl webInterfaceimpl=retrofitHelper.create(WebInterfaceimpl.class);

        Call<StudentProfileStatus> listCall=webInterfaceimpl.getStudentProfile();
        listCall.enqueue(new Callback<StudentProfileStatus>() {
            @Override
            public void onResponse(Call<StudentProfileStatus> call, Response<StudentProfileStatus> response) {

                progressFragment.dismiss();

                if(response.body()!=null) {

                    if(response.body().getData().size()>0) {

                        showStudentList(response.body().getData());
                    }
                    else {

                        Toast.makeText(AddEntertainActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                    }

                }

                else {

                    Toast.makeText(AddEntertainActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<StudentProfileStatus> call, Throwable t) {

                progressFragment.dismiss();


                if(!NetConnection.isConnected(AddEntertainActivity.this))
                {
                    Toast.makeText(AddEntertainActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }


            }
        });


    }


    public void showStudentList(final List<StudentProfile>students)
    {

        List<String>strings=new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(AddEntertainActivity.this);
        builder.setTitle("Choose a student");
        ;

        for (StudentProfile student:students
        ) {

            strings.add(student.getUdcStudentName());

        }



        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtStudentSelection.setText(students.get(which).getUdcStudentName());

                studentid=students.get(which).getUdcStudentId();

               // showLessonPlanlist();

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

}
