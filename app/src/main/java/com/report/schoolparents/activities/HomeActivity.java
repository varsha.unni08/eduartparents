package com.report.schoolparents.activities;


import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.transition.Slide;
import android.transition.Transition;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.report.schoolparents.AppLiterals.Constants;
import com.report.schoolparents.R;
import com.report.schoolparents.adapters.HomeAdapter;
import com.report.schoolparents.adapters.StudentsAdapter;
import com.report.schoolparents.connectivity.NetConnection;
import com.report.schoolparents.pojo.Student;
import com.report.schoolparents.pojo.StudentProfile;
import com.report.schoolparents.pojo.StudentProfileStatus;
import com.report.schoolparents.pojo.TeacherProfile;
import com.report.schoolparents.pojo.TeacherProfileStatus;
import com.report.schoolparents.preferencehelper.PreferenceHelper;
import com.report.schoolparents.progress.ProgressFragment;
import com.report.schoolparents.webservice.RetrofitHelper;
import com.report.schoolparents.webservice.WebInterfaceimpl;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class HomeActivity extends AppCompatActivity {

    Toolbar toolbar;
    ViewPager viewpager;

    RecyclerView recycler_view;

    AppBarLayout app_bar;

    ImageView imgprofile,imgentertain,imglogout;

    TabLayout tab_layout;

    ProgressFragment progressFragment;

    List<StudentProfile>studentProfiles;

    StudentProfile studentProfile;

    FloatingActionButton fab;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        toolbar=findViewById(R.id.toolbar);
        viewpager=findViewById(R.id.viewpager);
        tab_layout=findViewById(R.id.tab_layout);
        app_bar=findViewById(R.id.app_bar);
        imgprofile=findViewById(R.id.imgprofile);
        imgentertain=findViewById(R.id.imgentertain);
        imglogout=findViewById(R.id.imglogout);

        studentProfiles=new ArrayList<>();

        fab=findViewById(R.id.fab);

        recycler_view=findViewById(R.id.recycler_view);
        setSupportActionBar(toolbar);


        setupHomeAdapter();

        getStudentList();

        imgentertain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(HomeActivity.this,EntertainmentActivity.class));
            }
        });

        imglogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                AlertDialog.Builder builder=new AlertDialog.Builder(HomeActivity.this);
                builder.setTitle(R.string.app_name);
                builder.setMessage("Do you want to logout from "+getString(R.string.app_name)+" ?");
                builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        startActivity(new Intent(HomeActivity.this,LoginActivity.class));

                        new PreferenceHelper(HomeActivity.this).clearData();


                        finish();
                    }
                });

                builder.setNegativeButton("no", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();

                    }
                });
                builder.show();





            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (studentProfile != null) {

                    getTeacherData(studentProfile.getUdcStudentId());


//                    Uri u = Uri.parse("tel:"+studentProfile.getUdc_teacher_mob());
//
//
//                    Intent i = new Intent(Intent.ACTION_DIAL, u);
//
//                    try {
//
//                        startActivity(i);
//                    } catch (SecurityException s) {
//
//                    }

                }else {

                    Toast.makeText(HomeActivity.this,"Didn't get student profile",Toast.LENGTH_SHORT).show();
                }

            }
        });

        app_bar.addOnOffsetChangedListener(new AppBarLayout.BaseOnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (Math.abs(verticalOffset) == appBarLayout.getTotalScrollRange()) {

                    viewpager.setVisibility(View.GONE);
                    tab_layout.setVisibility(View.GONE);
                    // Collapsed
                } else if (verticalOffset == 0) {

                    viewpager.setVisibility(View.VISIBLE);
                    tab_layout.setVisibility(View.VISIBLE);
                    // Expanded
                } else {
                    // Somewhere in between

                    viewpager.setVisibility(View.VISIBLE);
                    tab_layout.setVisibility(View.VISIBLE);
                }
            }
        });

        imgprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                startActivity(new Intent(HomeActivity.this,ProfileActivity.class));
            }
        });

        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {


                if(studentProfiles.size()>0)
                {
                    studentProfile=studentProfiles.get(position);

                    Constants.studentprofile=studentProfile;
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    public void getTeacherData(String id)
    {

        progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"cjkk");

        Retrofit retrofitHelper= RetrofitHelper.getRetrofitInstance(HomeActivity.this);

        WebInterfaceimpl webInterfaceimpl=retrofitHelper.create(WebInterfaceimpl.class);

        Call<TeacherProfileStatus> listCall=webInterfaceimpl.getTeacherMobile(id);
        listCall.enqueue(new Callback<TeacherProfileStatus>() {
            @Override
            public void onResponse(Call<TeacherProfileStatus> call, Response<TeacherProfileStatus> response) {

                progressFragment.dismiss();

                if(response.body()!=null) {

                    if(response.body().getStatus()==1) {

                        if(response.body().getData().size()>0) {

                            final TeacherProfile teacherProfile =response.body().getData().get(0);



                            AlertDialog.Builder builder1 = new AlertDialog.Builder(HomeActivity.this);
                            builder1.setMessage("Name : "+teacherProfile.getUdcTeacherName()+"\n\n"+"Phone number : "+teacherProfile.getUdcTeacherMob());
                            builder1.setCancelable(true);

                            builder1.setPositiveButton(
                                    "Call now",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();

                                            Uri u = Uri.parse("tel:"+teacherProfile.getUdcTeacherMob());


                                            Intent i = new Intent(Intent.ACTION_DIAL, u);

                                            try {

                                                startActivity(i);
                                            } catch (SecurityException s) {

                                            }
                                        }
                                    });


                            AlertDialog alert11 = builder1.create();
                            alert11.show();

                            // showStudentList(response.body());
//                        studentProfiles.addAll(response.body().getData());
//
//                        viewpager.setAdapter(new StudentsAdapter(HomeActivity.this,response.body().getData()));
//                        tab_layout.setupWithViewPager(viewpager);
//
//                        studentProfile=studentProfiles.get(0);


                        }
                        else{

                            Toast.makeText(HomeActivity.this,"No data found",Toast.LENGTH_SHORT).show();
                        }


                    }
                    else {

                        Toast.makeText(HomeActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                    }

                }

                else {

                    Toast.makeText(HomeActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<TeacherProfileStatus> call, Throwable t) {

                progressFragment.dismiss();


                if(!NetConnection.isConnected(HomeActivity.this))
                {
                    Toast.makeText(HomeActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }


            }
        });


    }



    void setupHomeAdapter() {

        AnimationSet set = new AnimationSet(true);




        Animation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(500);
        set.addAnimation(animation);

        animation = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF, 0.0f
        );
        animation.setDuration(100);
        set.addAnimation(animation);

        LayoutAnimationController controller = new LayoutAnimationController(set, 0.5f);

        recycler_view.setLayoutManager(new GridLayoutManager(HomeActivity.this, 2));
        recycler_view.setLayoutAnimation(controller);
        recycler_view.setAdapter(new HomeAdapter(HomeActivity.this));
    }


    public void getStudentList()
    {

        progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"cjkk");

        Retrofit retrofitHelper= RetrofitHelper.getRetrofitInstance(HomeActivity.this);

        WebInterfaceimpl webInterfaceimpl=retrofitHelper.create(WebInterfaceimpl.class);

        Call<StudentProfileStatus> listCall=webInterfaceimpl.getStudentProfile();
        listCall.enqueue(new Callback<StudentProfileStatus>() {
            @Override
            public void onResponse(Call<StudentProfileStatus> call, Response<StudentProfileStatus> response) {

                progressFragment.dismiss();

                if(response.body()!=null) {

                    if(response.body().getStatus()==1) {

                       // showStudentList(response.body());

                        if(response.body().getData()!=null) {
                            studentProfiles.addAll(response.body().getData());

                            viewpager.setAdapter(new StudentsAdapter(HomeActivity.this, response.body().getData()));
                            tab_layout.setupWithViewPager(viewpager);

                            studentProfile = studentProfiles.get(0);
                            Constants.studentprofile = studentProfile;
                        }



                    }
                    else {

                        Toast.makeText(HomeActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                    }

                }

                else {

                    Toast.makeText(HomeActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<StudentProfileStatus> call, Throwable t) {

                progressFragment.dismiss();


                if(!NetConnection.isConnected(HomeActivity.this))
                {
                    Toast.makeText(HomeActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }


            }
        });


    }
}
