package com.report.schoolparents.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.report.schoolparents.R;
import com.report.schoolparents.connectivity.NetConnection;
import com.report.schoolparents.pojo.Student;
import com.report.schoolparents.pojo.StudentProfile;
import com.report.schoolparents.pojo.StudentProfileStatus;
import com.report.schoolparents.progress.ProgressFragment;
import com.report.schoolparents.webservice.RetrofitHelper;
import com.report.schoolparents.webservice.WebInterfaceimpl;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class AddLeaveActivity extends AppCompatActivity {

    ImageView imgback;

    Button btnSubmit;

    TextView txtStartdate,txttoDate,txtStudentSelection;

    AppCompatImageView imgpickFromDate,imgpickToDate;

    ImageView imgselectstudent;

    EditText edtReason;

    String start_date="",end_date="",studentid="";

    ProgressFragment progressFragment;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_leave);
        getSupportActionBar().hide();
        imgback=findViewById(R.id.imgback);
        btnSubmit=findViewById(R.id.btnSubmit);
        txtStartdate=findViewById(R.id.txtStartdate);
        imgpickFromDate=findViewById(R.id.imgpickFromDate);
        imgpickToDate=findViewById(R.id.imgpickToDate);
        txtStudentSelection=findViewById(R.id.txtStudentSelection);
        txttoDate=findViewById(R.id.txttoDate);
        imgselectstudent=findViewById(R.id.imgselectstudent);
        edtReason=findViewById(R.id.edtReason);


        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                onBackPressed();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (!start_date.equals("")) {

                    if (!end_date.equals("")) {

                        Date sta_date = null;
                        Date end_dat = null;


                        try {

                            DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
                            sta_date = dateFormat.parse(start_date);
                            end_dat = dateFormat.parse(end_date);

                            if (sta_date.before(end_dat)) {

                                if (!studentid.equals("")) {


                                    if (!edtReason.getText().toString().equals("")) {



                                        applyLeave();

                                    }
                                    else {

                                        Toast.makeText(AddLeaveActivity.this,"enter a reason",Toast.LENGTH_SHORT).show();

                                    }




                                }
                                else {

                                    Toast.makeText(AddLeaveActivity.this,"select a student",Toast.LENGTH_SHORT).show();

                                }




                            }

                            else {
                                Toast.makeText(AddLeaveActivity.this,"select date properly",Toast.LENGTH_SHORT).show();

                            }



                        }catch (Exception e)
                        {

                        }






                    }

                    else {
                        Toast.makeText(AddLeaveActivity.this,"select end date",Toast.LENGTH_SHORT).show();

                    }

                }else {

                    Toast.makeText(AddLeaveActivity.this,"select start date",Toast.LENGTH_SHORT).show();

                }




            }
        });

        imgpickToDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDatePicker(0);

            }
        });

        imgpickFromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDatePicker(1);

            }
        });

        txtStartdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(1);
            }
        });

        txttoDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(0);
            }
        });


        txtStudentSelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getStudentList();

            }
        });

        imgselectstudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getStudentList();
            }
        });
    }


    public void showDatePicker(final int start) {
        final Calendar cldr = Calendar.getInstance();
        int day = cldr.get(Calendar.DAY_OF_MONTH);
        int month = cldr.get(Calendar.MONTH);
        int year = cldr.get(Calendar.YEAR);

        DatePickerDialog datePickerDialog = new DatePickerDialog(AddLeaveActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                // txtDob.setText();

                int m = i1 + 1;

                if (start == 1) {
                    start_date = i + "-" + m + "-" + i2;
                    txtStartdate.setText(start_date);
                } else {
                    end_date = i + "-" + m + "-" + i2;
                    txttoDate.setText(end_date);
                }


            }
        }, year, month, day);

        datePickerDialog.show();
    }


    public void getStudentList()
    {

        progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"cjkk");

        Retrofit retrofitHelper= RetrofitHelper.getRetrofitInstance(AddLeaveActivity.this);

        WebInterfaceimpl webInterfaceimpl=retrofitHelper.create(WebInterfaceimpl.class);

        Call<StudentProfileStatus> listCall=webInterfaceimpl.getStudentProfile();
        listCall.enqueue(new Callback<StudentProfileStatus>() {
            @Override
            public void onResponse(Call<StudentProfileStatus> call, Response<StudentProfileStatus> response) {

                progressFragment.dismiss();

                if(response.body()!=null) {

                    if(response.body().getData().size()>0) {

                        showStudentList(response.body().getData());
                    }
                    else {

                        Toast.makeText(AddLeaveActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                    }

                }

                else {

                    Toast.makeText(AddLeaveActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<StudentProfileStatus> call, Throwable t) {

                progressFragment.dismiss();


                if(!NetConnection.isConnected(AddLeaveActivity.this))
                {
                    Toast.makeText(AddLeaveActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }


            }
        });


    }


    public void showStudentList(final List<StudentProfile>students)
    {

        List<String>strings=new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(AddLeaveActivity.this);
        builder.setTitle("Choose a student");
        ;

        for (StudentProfile student:students
        ) {

            strings.add(student.getUdcStudentName());

        }



        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtStudentSelection.setText(students.get(which).getUdcStudentName());

                studentid=students.get(which).getUdcStudentId();

                //getLeaveList(studentid);

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    public void applyLeave()
    {
        progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"cjkk");

        Retrofit retrofitHelper= RetrofitHelper.getRetrofitInstance(AddLeaveActivity.this);

        WebInterfaceimpl webInterfaceimpl=retrofitHelper.create(WebInterfaceimpl.class);

        final Call<JsonArray>jsonArrayCall=webInterfaceimpl.applyParentLeave(studentid,txtStudentSelection.getText().toString(),start_date,end_date,edtReason.getText().toString());

        jsonArrayCall.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {

                progressFragment.dismiss();

                if(response.body()!=null) {

                    if(response.body().size()>0) {



                        try{

                            JSONArray jsonArray=new JSONArray(response.body().toString());


                                JSONObject jsonObject=jsonArray.getJSONObject(0);
                                Toast.makeText(AddLeaveActivity.this,jsonObject.getString("message"),Toast.LENGTH_SHORT).show();






                        }catch (Exception e)
                        {


                        }




                    }
                    else {

                        Toast.makeText(AddLeaveActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                    }

                }

                else {

                    Toast.makeText(AddLeaveActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                progressFragment.dismiss();


                if(!NetConnection.isConnected(AddLeaveActivity.this))
                {
                    Toast.makeText(AddLeaveActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }
            }
        });


    }
}
