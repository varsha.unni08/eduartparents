package com.report.schoolparents.activities;


import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.report.schoolparents.AppLiterals.Constants;
import com.report.schoolparents.R;
import com.report.schoolparents.adapters.AttendanceAdapter;
import com.report.schoolparents.connectivity.NetConnection;
import com.report.schoolparents.pojo.Attendance;
import com.report.schoolparents.pojo.AttendanceData;
import com.report.schoolparents.pojo.AttendanceStatus;
import com.report.schoolparents.pojo.Student;
import com.report.schoolparents.pojo.StudentProfile;
import com.report.schoolparents.pojo.StudentProfileStatus;
import com.report.schoolparents.progress.ProgressFragment;
import com.report.schoolparents.webservice.RetrofitHelper;
import com.report.schoolparents.webservice.WebInterfaceimpl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class AttendanceActivity extends AppCompatActivity {


    ImageView imgback, imgdate;
    TextView txtdate;

    boolean isFirstday = false;

    RecyclerView recyclerView;
    TextView txtStudentSelection,txtTotalLeaves;

    ImageView imgselectstudent;

    ProgressFragment progressFragment;

    String studentid = "";

    Button btnShowAttendance;

    Integer month=0, year=0;
    LinearLayout layout_week;

    List<Attendance> attendanceList;

    int totalleaves=0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance);
        getSupportActionBar().hide();
        imgback = findViewById(R.id.imgback);
        imgdate = findViewById(R.id.imgdate);
        txtdate = findViewById(R.id.txtdate);
        recyclerView = findViewById(R.id.recyclerView);
        txtStudentSelection = findViewById(R.id.txtStudentSelection);
        imgselectstudent = findViewById(R.id.imgselectstudent);
        btnShowAttendance = findViewById(R.id.btnShowAttendance);
        txtTotalLeaves=findViewById(R.id.txtTotalLeaves);
        layout_week = findViewById(R.id.layout_week);

        attendanceList = new ArrayList<>();


        btnShowAttendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (month != 0 && year != 0) {

                    if (!studentid.equalsIgnoreCase("")) {

                        totalleaves=0;


                        getAttendance();

                    } else {

                        Toast.makeText(AttendanceActivity.this, "Select a student", Toast.LENGTH_SHORT).show();

                    }


                } else {

                    Toast.makeText(AttendanceActivity.this, "Select month and year", Toast.LENGTH_SHORT).show();

                }


            }
        });

        if(Constants.studentprofile!=null)
        {
            txtStudentSelection.setText("Student name : "+Constants.studentprofile.getUdcStudentName());
            studentid=Constants.studentprofile.getUdcStudentId();
        }


//        txtStudentSelection.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                getStudentList();
//
//            }
//        });
//
//        imgselectstudent.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                getStudentList();
//
//            }
//        });

        imgdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ExpiryDialog();

            }
        });

        txtdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ExpiryDialog();

            }
        });

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });
    }


    public void getStudentList() {

        progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "cjkk");

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(AttendanceActivity.this);

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

        Call<StudentProfileStatus> listCall=webInterfaceimpl.getStudentProfile();
        listCall.enqueue(new Callback<StudentProfileStatus>() {
            @Override
            public void onResponse(Call<StudentProfileStatus> call, Response<StudentProfileStatus> response) {

                progressFragment.dismiss();

                if (response.body() != null) {

                    if (response.body().getData().size() > 0) {

                        showStudentList(response.body().getData());
                    } else {

                        Toast.makeText(AttendanceActivity.this, "No data found", Toast.LENGTH_SHORT).show();

                    }

                } else {

                    Toast.makeText(AttendanceActivity.this, "No data found", Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<StudentProfileStatus> call, Throwable t) {

                progressFragment.dismiss();


                if (!NetConnection.isConnected(AttendanceActivity.this)) {
                    Toast.makeText(AttendanceActivity.this, "Check Internet connection", Toast.LENGTH_SHORT).show();
                }


            }
        });


    }


    public void showStudentList(final List<StudentProfile> students) {

        List<String> strings = new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(AttendanceActivity.this);
        builder.setTitle("Choose a student");
        ;

        for (StudentProfile student : students
        ) {

            strings.add(student.getUdcStudentName());

        }


        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtStudentSelection.setText(students.get(which).getUdcStudentName());

                studentid = students.get(which).getUdcStudentId();

                //showLessonPlanlist();

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    private void ExpiryDialog() {

        final Dialog dialog = new Dialog(AttendanceActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_date_picker);
        dialog.show();

        Calendar mCalendar = Calendar.getInstance();

        final DatePicker datePicker = (DatePicker) dialog.findViewById(R.id.date_picker);
        Button date_time_set = (Button) dialog.findViewById(R.id.date_time_set);
        datePicker.init(mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH), null);

        datePicker.setSpinnersShown(true);

        // hiding calendarview and daySpinner in datePicker
        datePicker.setCalendarViewShown(false);

        LinearLayout pickerParentLayout = (LinearLayout) datePicker.getChildAt(0);

        LinearLayout pickerSpinnersHolder = (LinearLayout) pickerParentLayout.getChildAt(0);

        pickerSpinnersHolder.getChildAt(1).setVisibility(View.GONE);

//        LinearLayout ll = (LinearLayout) datePicker.getChildAt(0);
//        LinearLayout ll2 = (LinearLayout) ll.getChildAt(0);
//        ll2.getChildAt(0).setVisibility(View.INVISIBLE);

        date_time_set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                isFirstday = false;

                try {
                    month = datePicker.getMonth() + 1;
                    year = datePicker.getYear();

                    String expMonth = (month.toString().length() == 1 ? "0" + month.toString() : month.toString());
                    String expYear = year.toString();
                    txtdate.setText(expMonth + "/" + expYear);


                } catch (Exception e) {

                }

            }
        });
    }


    public void getAttendance() {
        progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "cjkk");

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(AttendanceActivity.this);

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

        Call<AttendanceStatus>listCall=webInterfaceimpl.getStudentLeaveByParent(studentid,String.valueOf(month), String.valueOf(year));


        listCall.enqueue(new Callback<AttendanceStatus>() {
            @Override
            public void onResponse(Call<AttendanceStatus> call, Response<AttendanceStatus> response) {
                progressFragment.dismiss();

                if (response.body() != null) {

                    if (response.body().getData().size() > 0) {

                        attendanceList.clear();

                       // attendanceList.addAll(response.body());

                       // txtTotalLeaves.setText("Total No. of leaves : "+response.body().size());

                        AttendanceData adt=response.body().getData().get(0);

                        String fromdate=adt.getUdcFromDate();

                        String todate=adt.getUdcToDate();

                        SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
                        try {
                            Date fdate = inFormat.parse(fromdate);

                            Date tdate = inFormat.parse(todate);



                            Calendar c=Calendar.getInstance();
                            c.setTime(fdate);
//                            String startdate=inFormat.format(c.getTime());
//
//                            Attendance ate=new Attendance();
//                            ate.setUdcAbsenceDate(startdate);
//                            ate.setUdcComments(adt.getUdcReason());
//                            attendanceList.add(ate);

                            while (!fdate.after(tdate))
                            {


                                String startdate1=inFormat.format(fdate);

                                Attendance ate1=new Attendance();
                                ate1.setUdcAbsenceDate(startdate1);
                                ate1.setUdcComments(adt.getUdcReason());
                                attendanceList.add(ate1);
                                c.add(Calendar.DAY_OF_MONTH,1);
                                fdate=c.getTime();
                            }



                        } catch (ParseException e) {
                            e.printStackTrace();
                            Log.e("error",e.toString());
                        }



                        txtTotalLeaves.setText("Total No. of leaves : "+attendanceList.size());


                        setCalendarForAttendance();




                    } else {
                        recyclerView.setVisibility(View.GONE);
                        layout_week.setVisibility(View.GONE);
                        txtTotalLeaves.setVisibility(View.GONE);

                        setCalendar();

                        Toast.makeText(AttendanceActivity.this, "No data found", Toast.LENGTH_SHORT).show();

                    }

                } else {

                    recyclerView.setVisibility(View.GONE);
                    layout_week.setVisibility(View.GONE);
                    txtTotalLeaves.setVisibility(View.GONE);

                    setCalendar();

                    Toast.makeText(AttendanceActivity.this, "No data found", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<AttendanceStatus> call, Throwable t) {

                progressFragment.dismiss();

                if (!NetConnection.isConnected(AttendanceActivity.this)) {
                    Toast.makeText(AttendanceActivity.this, "Check Internet connection", Toast.LENGTH_SHORT).show();
                }

            }
        });


    }


    public void setCalendar() {
        try {

            isFirstday = false;

            recyclerView.setVisibility(View.VISIBLE);
            layout_week.setVisibility(View.VISIBLE);

            txtTotalLeaves.setVisibility(View.VISIBLE);
//            month = 7;
//            year = 2018;

            totalleaves = 0;


            String expMonth = (month.toString().length() == 1 ? "0" + month.toString() : month.toString());
            String expYear = year.toString();
            txtdate.setText(expMonth + "/" + expYear);

            SimpleDateFormat inFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date date = inFormat.parse("01-" + month + "-" + year);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            String[] days = new String[]{"", "SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY"};
            String day = days[calendar.get(Calendar.DAY_OF_WEEK)];

            String[] daysloop = new String[]{"SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY"};

            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.YEAR, year);
            cal.set(Calendar.MONTH, month);
            int numDays = calendar.getActualMaximum(Calendar.DATE);


//                    Map<Integer, List<Attendance>> integerListMap=new HashMap<>();
            List<Attendance> attendances = new ArrayList<>();


            if (!isFirstday) {

                for (int k = 0; k < daysloop.length; k++) {

                    if (daysloop[k].equals(day)) {
                        isFirstday = true;

                        for (int i = 0; i < numDays; i++) {

                            int a = i + 1;
                            String d = "";

                            if (a < 10) {
                                d = "0" + a;
                            } else {
                                d = "" + a;
                            }

                            Attendance attendance = new Attendance();
                            attendance.setIndex(0);
                            attendance.setData(String.valueOf(a));
                            attendance.setDate(expYear + "-" + expMonth + "-" + d);
                            attendances.add(attendance);
                        }
                        break;

                    } else {

                        Attendance attendance = new Attendance();
                        attendance.setIndex(-1);
                        attendance.setData("");
                        attendances.add(attendance);
                    }


                }


            }

            txtTotalLeaves.setText("Total No. of leaves : 0");

            recyclerView.setLayoutManager(new GridLayoutManager(AttendanceActivity.this, 7));
            recyclerView.setAdapter(new AttendanceAdapter(AttendanceActivity.this, attendances));
        } catch (Exception e) {

        }
    }


    public void setCalendarForAttendance() {
        try {

            isFirstday=false;

            recyclerView.setVisibility(View.VISIBLE);
            layout_week.setVisibility(View.VISIBLE);

            txtTotalLeaves.setVisibility(View.VISIBLE);
//            month = 7;
//            year = 2018;

            totalleaves=0;


            String expMonth = (month.toString().length() == 1 ? "0" + month.toString() : month.toString());
            String expYear = year.toString();
            txtdate.setText(expMonth + "/" + expYear);

            SimpleDateFormat inFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date date = inFormat.parse("01-" + month + "-" + year);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            String[] days = new String[]{"", "SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY"};
            String day = days[calendar.get(Calendar.DAY_OF_WEEK)];

            String[] daysloop = new String[]{"SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY"};

            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.YEAR, year);
            cal.set(Calendar.MONTH, month);
            int numDays = calendar.getActualMaximum(Calendar.DATE);


//                    Map<Integer, List<Attendance>> integerListMap=new HashMap<>();
            List<Attendance> attendances = new ArrayList<>();


            if (!isFirstday) {

                for (int k = 0; k < daysloop.length; k++) {

                    if (daysloop[k].equals(day)) {
                        isFirstday = true;

                        for (int i = 0; i < numDays; i++) {

                            int a = i + 1;
                            String d = "";

                            if (a < 10) {
                                d = "0" + a;
                            } else {
                                d = "" + a;
                            }

                            Attendance attendance = new Attendance();
                            attendance.setIndex(0);
                            attendance.setData(String.valueOf(a));
                            attendance.setDate(expYear + "-" + expMonth + "-" + d);
                            attendances.add(attendance);
                        }
                        break;

                    } else {

                        Attendance attendance = new Attendance();
                        attendance.setIndex(-1);
                        attendance.setData("");
                        attendances.add(attendance);
                    }


                }


            }


            for (Attendance attendance : attendanceList) {


                for (Attendance att1 : attendances) {


                    try {



                            if (att1.getDate().trim().equalsIgnoreCase(attendance.getUdcAbsenceDate().trim())) {




                                att1.setUdcAbsenceDate(attendance.getUdcAbsenceDate());

                                att1.setUdcComments(attendance.getUdcComments());
                                att1.setIsleave(true);
                            }



                    } catch (Exception e) {

                    }


                }

            }





            recyclerView.setLayoutManager(new GridLayoutManager(AttendanceActivity.this, 7));
            recyclerView.setAdapter(new AttendanceAdapter(AttendanceActivity.this, attendances));

        } catch (Exception e) {

        }

    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();

        ActivityCompat.finishAfterTransition(this);
    }
}
