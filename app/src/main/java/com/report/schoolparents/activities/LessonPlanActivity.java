package com.report.schoolparents.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.report.schoolparents.AppLiterals.Constants;
import com.report.schoolparents.R;
import com.report.schoolparents.adapters.LessonPlanAdapter;
import com.report.schoolparents.connectivity.NetConnection;
import com.report.schoolparents.pojo.LessonPlan;
import com.report.schoolparents.pojo.LessonPlanStatus;
import com.report.schoolparents.pojo.Student;
import com.report.schoolparents.pojo.StudentProfile;
import com.report.schoolparents.pojo.StudentProfileStatus;
import com.report.schoolparents.progress.ProgressFragment;
import com.report.schoolparents.webservice.RetrofitHelper;
import com.report.schoolparents.webservice.WebInterfaceimpl;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class LessonPlanActivity extends AppCompatActivity {

    RecyclerView recyclerView;

    TextView txtStudentSelection;

    ImageView imgselectstudent,imgback;

    ProgressFragment progressFragment;

    String studentid="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lesson_plan);
        getSupportActionBar().hide();
        recyclerView=findViewById(R.id.recyclerView);
        imgback=findViewById(R.id.imgback);
        txtStudentSelection=findViewById(R.id.txtStudentSelection);
        imgselectstudent=findViewById(R.id.imgselectstudent);

//        txtStudentSelection.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                getStudentList();
//
//            }
//        });
//
//        imgselectstudent.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                getStudentList();
//            }
//        });

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        if(Constants.studentprofile!=null)
        {
            txtStudentSelection.setText("Selected student : "+Constants.studentprofile.getUdcStudentName());

            studentid=Constants.studentprofile.getUdcStudentId();
        }
        showLessonPlanlist();

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ActivityCompat.finishAfterTransition(this);
    }

    public void getStudentList()
    {

        progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"cjkk");

        Retrofit retrofitHelper= RetrofitHelper.getRetrofitInstance(LessonPlanActivity.this);

        WebInterfaceimpl webInterfaceimpl=retrofitHelper.create(WebInterfaceimpl.class);

        Call<StudentProfileStatus> listCall=webInterfaceimpl.getStudentProfile();
        listCall.enqueue(new Callback<StudentProfileStatus>() {
            @Override
            public void onResponse(Call<StudentProfileStatus> call, Response<StudentProfileStatus> response) {

                progressFragment.dismiss();

                if(response.body()!=null) {

                    if(response.body().getData().size()>0) {

                        showStudentList(response.body().getData());
                    }
                    else {

                        Toast.makeText(LessonPlanActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                    }

                }

                else {

                    Toast.makeText(LessonPlanActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<StudentProfileStatus> call, Throwable t) {

                progressFragment.dismiss();


                if(!NetConnection.isConnected(LessonPlanActivity.this))
                {
                    Toast.makeText(LessonPlanActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }


            }
        });


    }


    public void showStudentList(final List<StudentProfile>students)
    {

        List<String>strings=new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(LessonPlanActivity.this);
        builder.setTitle("Choose a student");
        ;

        for (StudentProfile student:students
        ) {

            strings.add(student.getUdcStudentName());

        }



        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtStudentSelection.setText(students.get(which).getUdcStudentName());

                studentid=students.get(which).getUdcStudentId();

                showLessonPlanlist();

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    public void showLessonPlanlist()
    {


        progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"cjkk");

        Retrofit retrofitHelper=RetrofitHelper.getRetrofitInstance(LessonPlanActivity.this);

        WebInterfaceimpl webInterfaceimpl=retrofitHelper.create(WebInterfaceimpl.class);

        Call<LessonPlanStatus>listCall=webInterfaceimpl.getAllLessonplans(studentid);

        listCall.enqueue(new Callback<LessonPlanStatus>() {
            @Override
            public void onResponse(Call<LessonPlanStatus> call, Response<LessonPlanStatus> response) {

                progressFragment.dismiss();

                if(response.body()!=null)
                {
                    if(response.body().getData().size()>0)
                    {

                        recyclerView.setVisibility(View.VISIBLE);

                        recyclerView.setLayoutManager(new LinearLayoutManager(LessonPlanActivity.this));
                        recyclerView.setAdapter(new LessonPlanAdapter(LessonPlanActivity.this,response.body().getData()));



                    }
                    else {

                        recyclerView.setVisibility(View.GONE);

                        Toast.makeText(LessonPlanActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                    }




                }

            }

            @Override
            public void onFailure(Call<LessonPlanStatus> call, Throwable t) {
                progressFragment.dismiss();


                if(!NetConnection.isConnected(LessonPlanActivity.this))
                {
                    Toast.makeText(LessonPlanActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }
            }
        });





    }
}
