package com.report.schoolparents.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.report.schoolparents.AppLiterals.Constants;
import com.report.schoolparents.R;
import com.report.schoolparents.adapters.MarklistAdapter;
import com.report.schoolparents.connectivity.NetConnection;
import com.report.schoolparents.pojo.MarkList;
import com.report.schoolparents.pojo.Student;
import com.report.schoolparents.pojo.StudentProfile;
import com.report.schoolparents.pojo.StudentProfileStatus;
import com.report.schoolparents.progress.ProgressFragment;
import com.report.schoolparents.webservice.RetrofitHelper;
import com.report.schoolparents.webservice.WebInterfaceimpl;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MarkListActivity extends AppCompatActivity {

    ImageView imgBack,imgselectstudent,imgselectExam,imgSelectTerm;

    RecyclerView recyclerview;

    TextView txtStudentSelection,txtExamSelection,txtTerm,txtTotal;

    Button btnSubmit;

    ProgressFragment progressFragment;

    String studentid="";

    String examtype="",term="";

    LinearLayout layout_head;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mark_list);
        getSupportActionBar().hide();
        imgBack=findViewById(R.id.imgBack);
        imgselectstudent=findViewById(R.id.imgselectstudent);
        imgselectExam=findViewById(R.id.imgselectExam);
        imgSelectTerm=findViewById(R.id.imgSelectTerm);
        btnSubmit=findViewById(R.id.btnSubmit);

        layout_head=findViewById(R.id.layout_head);

        txtStudentSelection=findViewById(R.id.txtStudentSelection);
        txtExamSelection=findViewById(R.id.txtExamSelection);
        txtTerm=findViewById(R.id.txtTerm);
        txtTotal=findViewById(R.id.txtTotal);

        recyclerview=findViewById(R.id.recyclerview);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!studentid.equals("")) {

                    if(!examtype.equals("")){

                        if(!term.equals(""))
                        {


                    getMarkList();

                        }
                        else {

                            Toast.makeText(MarkListActivity.this,"select term",Toast.LENGTH_SHORT).show();

                        }
                    }
                    else {

                        Toast.makeText(MarkListActivity.this,"select exam type",Toast.LENGTH_SHORT).show();

                    }
                }
                else {

                    Toast.makeText(MarkListActivity.this,"select student",Toast.LENGTH_SHORT).show();

                }


            }
        });

//        imgselectstudent.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                getStudentList();
//
//            }
//        });
//
//        txtStudentSelection.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                getStudentList();
//
//            }
//        });
//


if(Constants.studentprofile!=null)
{
    txtStudentSelection.setText("Selected student : "+Constants.studentprofile.getUdcStudentName());

    studentid=Constants.studentprofile.getUdcStudentId();
}





        imgselectExam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showExamList();



            }
        });

        txtExamSelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                showExamList();

            }
        });







        imgSelectTerm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showTermList();



            }
        });

        txtTerm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                showTermList();

            }
        });



    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ActivityCompat.finishAfterTransition(this);
       // finish();
    }


    public void getMarkList()
    {
        progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"cjkk");

        Retrofit retrofitHelper=RetrofitHelper.getRetrofitInstance(MarkListActivity.this);

        WebInterfaceimpl webInterfaceimpl=retrofitHelper.create(WebInterfaceimpl.class);

        Call<List<MarkList>>markListCall=webInterfaceimpl.getMarkList(studentid,examtype,term);


        markListCall.enqueue(new Callback<List<MarkList>>() {
            @Override
            public void onResponse(Call<List<MarkList>> call, Response<List<MarkList>> response) {

                progressFragment.dismiss();


                if(response.body()!=null) {

                    if(response.body().size()>0) {

                        showMarkList(response.body());

                    }
                    else {

                        layout_head.setVisibility(View.GONE);
                        recyclerview.setVisibility(View.GONE);
                        txtTotal.setText("");

                        Toast.makeText(MarkListActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                    }

                }

                else {

                    layout_head.setVisibility(View.GONE);
                    recyclerview.setVisibility(View.GONE);

                    txtTotal.setText("");

                    Toast.makeText(MarkListActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                }




            }

            @Override
            public void onFailure(Call<List<MarkList>> call, Throwable t) {

                progressFragment.dismiss();


                if(!NetConnection.isConnected(MarkListActivity.this))
                {
                    Toast.makeText(MarkListActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }



    public void showMarkList(List<MarkList>markLists)
    {


       // MarkList lastmarklist=markLists.get(markLists.size()-1);

//        txtTotal.setText("Grand total : "+lastmarklist.getGrandTotal()+"\nPercentage : "+lastmarklist.getPercentage());


       // markLists.remove(markLists.size()-1);

        if(markLists.size()>0)
        {


            layout_head.setVisibility(View.VISIBLE);
            recyclerview.setVisibility(View.VISIBLE);


            recyclerview.setLayoutManager(new LinearLayoutManager(MarkListActivity.this));
            recyclerview.setAdapter(new MarklistAdapter(MarkListActivity.this,markLists));

        }
        else {

            layout_head.setVisibility(View.GONE);
            recyclerview.setVisibility(View.GONE);

            txtTotal.setText("");

            Toast.makeText(MarkListActivity.this,"No data found",Toast.LENGTH_SHORT).show();

        }










    }



    public void getStudentList()
    {

        progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"cjkk");

        Retrofit retrofitHelper=RetrofitHelper.getRetrofitInstance(MarkListActivity.this);

        WebInterfaceimpl webInterfaceimpl=retrofitHelper.create(WebInterfaceimpl.class);

        Call<StudentProfileStatus>listCall=webInterfaceimpl.getStudentProfile();
        listCall.enqueue(new Callback<StudentProfileStatus>() {
            @Override
            public void onResponse(Call<StudentProfileStatus> call, Response<StudentProfileStatus> response) {

                progressFragment.dismiss();

                if(response.body()!=null) {

                    if(response.body().getData().size()>0) {

                        showStudentList(response.body().getData());
                    }
                    else {

                        Toast.makeText(MarkListActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                    }

                }

                else {

                    Toast.makeText(MarkListActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<StudentProfileStatus> call, Throwable t) {

                progressFragment.dismiss();


                if(!NetConnection.isConnected(MarkListActivity.this))
                {
                    Toast.makeText(MarkListActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }


            }
        });


    }


    public void showStudentList(final List<StudentProfile>students)
    {

        List<String>strings=new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(MarkListActivity.this);
        builder.setTitle("Choose a student");
        ;

        for (StudentProfile student:students
             ) {

            strings.add(student.getUdcStudentName());

        }



        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtStudentSelection.setText(students.get(which).getUdcStudentName());

                studentid=students.get(which).getUdcStudentId();

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }



    public void showExamList()
    {


        final String arr[]={" Assessment tests","Main Exam"};

        AlertDialog.Builder builder = new AlertDialog.Builder(MarkListActivity.this);
        builder.setTitle("Choose an Exam");
        ;



        builder.setItems(arr, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtExamSelection.setText(arr[which]);

                int t=which+1;

                examtype=String.valueOf(t);

                term="";

                txtTerm.setText("Select term");




            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    public void showTermList()
    {

        final String arr[] =new String[3];

        if(examtype.equalsIgnoreCase("1")) {


         String[] a = {"1st term", "2nd term", "3rd term"};

         for(int i=0;i<a.length;i++)
         {
             arr[i]=a[i];
         }




        }
        else{
            String[] a = {"Quarterly", "Half yearly", "Annual exam"};

            for(int i=0;i<a.length;i++)
            {
                arr[i]=a[i];
            }


        }

        AlertDialog.Builder builder = new AlertDialog.Builder(MarkListActivity.this);
        builder.setTitle("Choose a Term");
        ;



        builder.setItems(arr, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtTerm.setText(arr[which]);
                if(which==0)
                {
                    term="1st";
                }
              else  if(which==1)
                {
                    term="2nd";
                }
              else  if(which==2)
                {
                    term="3rd";
                }

               // term=String.valueOf(which);

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
