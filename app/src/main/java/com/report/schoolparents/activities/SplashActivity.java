package com.report.schoolparents.activities;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.report.schoolparents.R;
import com.report.schoolparents.appconstants.Literals;
import com.report.schoolparents.preferencehelper.PreferenceHelper;

public class SplashActivity extends AppCompatActivity {


    Thread thread;

    ImageView imageView2;
    TextView textView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getSupportActionBar().hide();
        imageView2=findViewById(R.id.imageView2);
        textView=findViewById(R.id.textView);

        imageView2.setAlpha(0f);
        imageView2.setVisibility(View.VISIBLE);

        imageView2.animate()
                .alpha(1f)
                .setDuration(3000)
                .setListener(null);



        textView.setAlpha(0f);
        textView.setVisibility(View.VISIBLE);

        textView.animate()
                .alpha(1f)
                .setDuration(3000)
                .setListener(null);


        thread=new Thread(new Runnable() {
            @Override
            public void run() {

                try{
                    thread.sleep(3000);


                    if(!new PreferenceHelper(SplashActivity.this).getData(Literals.Logintokenkey).equals(""))

                    {
                        startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_in_right);

                        finish();
                    }
                    else {


                        startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_in_right);
                        finish();
                    }


                }catch (Exception e)
                {

                }

            }
        });

        thread.start();
    }



}
