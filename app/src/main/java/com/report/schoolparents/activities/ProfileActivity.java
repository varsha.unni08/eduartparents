package com.report.schoolparents.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.report.schoolparents.R;
import com.report.schoolparents.pojo.ParentProfile;
import com.report.schoolparents.progress.ProgressFragment;
import com.report.schoolparents.webservice.RetrofitHelper;
import com.report.schoolparents.webservice.WebInterfaceimpl;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ProfileActivity extends AppCompatActivity {

    ImageView imgprofile;

    ImageView imgback;

    ProgressFragment progressFragment;

    TextView txtName,txtEmail,txtMobile,txtQualification,txtOccupation,txtAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        getSupportActionBar().hide();

        imgprofile=findViewById(R.id.imgprofile);
        imgback=findViewById(R.id.imgback);

        txtQualification=findViewById(R.id.txtQualification);
        txtName=findViewById(R.id.txtName);
        txtEmail=findViewById(R.id.txtEmail);
        txtMobile=findViewById(R.id.txtMobile);
        txtOccupation=findViewById(R.id.txtOccupation);
        txtAddress=findViewById(R.id.txtAddress);

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        Glide.with(ProfileActivity.this)
                .load("")
                .apply(RequestOptions.circleCropTransform())
                .placeholder(R.drawable.parentplaceholder)
                .into(imgprofile);

        getParentProfile();


    }


    public void getParentProfile()
    {
        progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"cjkk");

        Retrofit retrofitHelper= RetrofitHelper.getRetrofitInstance(ProfileActivity.this);

        WebInterfaceimpl webInterfaceimpl=retrofitHelper.create(WebInterfaceimpl.class);

        Call<List<ParentProfile>>listCall=webInterfaceimpl.getParentProfile();

        listCall.enqueue(new Callback<List<ParentProfile>>() {
            @Override
            public void onResponse(Call<List<ParentProfile>> call, Response<List<ParentProfile>> response) {
                progressFragment.dismiss();

                if(response.body()!=null) {

                    if(response.body().size()>0) {

                        //showStudentList(response.body());

                        txtName.setText(response.body().get(0).getUdcFatherName());
                        txtMobile.setText(response.body().get(0).getUdcFatherContactMobile());
                        txtQualification.setText(response.body().get(0).getUdcFatherQualification());
                        txtEmail.setText(response.body().get(0).getUdcFatherEmailId());
                        txtOccupation.setText(response.body().get(0).getUdcFatherOccupation());
                        txtAddress.setText(response.body().get(0).getUdcFatherContactAddress());
                    }
                    else {

                        Toast.makeText(ProfileActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                    }

                }

                else {

                    Toast.makeText(ProfileActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<List<ParentProfile>> call, Throwable t) {
                Toast.makeText(ProfileActivity.this,"No data found",Toast.LENGTH_SHORT).show();
            }
        });
    }
}
