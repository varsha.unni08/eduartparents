package com.report.schoolparents.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.report.schoolparents.R;
import com.report.schoolparents.adapters.InstallmentAdapter;
import com.report.schoolparents.pojo.Installments;
import com.report.schoolparents.utils.Utilities;

import java.util.ArrayList;
import java.util.List;

import static com.report.schoolparents.utils.Utilities.arr;
import static com.report.schoolparents.utils.Utilities.fee;
import static com.report.schoolparents.utils.Utilities.feepaid;

public class FeeDetailsActivity extends AppCompatActivity {


    List<Installments>installments;
    RecyclerView recycler_view;
    ImageView imgback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fee_details);
        getSupportActionBar().hide();
        recycler_view=this.findViewById(R.id.recycler_view);
        installments=new ArrayList<>();
        imgback=findViewById(R.id.imgback);

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });

        for(int i=0;i< arr.length;i++)
        {
            Installments inst=new Installments();
            inst.setFeename(arr[i]);
            inst.setFees(fee[i]);
            inst.setStatus(feepaid[i]);

            installments.add(inst);

        }

        InstallmentAdapter installmentAdapter=new InstallmentAdapter(FeeDetailsActivity.this,installments);
        recycler_view.setLayoutManager(new LinearLayoutManager(FeeDetailsActivity.this));
        recycler_view.setAdapter(installmentAdapter);



    }
}