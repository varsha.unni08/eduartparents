package com.report.schoolparents.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.app.ActivityCompat;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.report.schoolparents.R;
import com.report.schoolparents.pojo.Entertainments;

public class EntertainDetailActivity extends AppCompatActivity {

    ImageView imgback,imgEntertain;

    TextView txtDescription,txtName,txtVideolink,txtPhotolink;

    Entertainments entertainments;





    Handler handler;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entertain_detail);
        getSupportActionBar().hide();

        entertainments=(Entertainments)getIntent().getSerializableExtra("Entertainment");

        handler=new Handler();

        imgback=findViewById(R.id.imgback);
        imgEntertain=findViewById(R.id.imgEntertain);
        txtName=findViewById(R.id.txtName);
        txtDescription=findViewById(R.id.txtDescription);
        txtVideolink=findViewById(R.id.txtVideolink);
        txtPhotolink=findViewById(R.id.txtPhotolink);



        txtName.setText(entertainments.getDsEntertainmentName());
        txtDescription.setText(entertainments.getDsEntertainmentDescription());
        txtVideolink.setText(entertainments.getDsVideo());
        txtPhotolink.setText(entertainments.getDsImage());

        if(entertainments.getDsImage()!=null) {
            Glide.with(EntertainDetailActivity.this)
                    .load(entertainments.getDsImage())

                    .placeholder(R.drawable.ic_launcher)
                    .into(imgEntertain);
        }
//
//
//        if(entertainments.getDsImage().contains(".jpg")) {
//
//
//            DisplayMetrics displayMetrics = new DisplayMetrics();
//            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
//
//            int margin = (int) getResources().getDimension(R.dimen.dimen_10dp);
//
//            int width = displayMetrics.widthPixels;
//
//            double heght = width / 1.5;
//            int h = (int) heght;
//
//            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) capturedimg.getLayoutParams();
//
//            layoutParams.width = width;
//            layoutParams.height = h;
//
//            layoutParams.setMargins(margin, margin, margin, margin);
//            capturedimg.setLayoutParams(layoutParams);
//
//            Glide.with(EntertainDetailActivity.this)
//                    .load(entertainments.getDsImage())
//
//                    .placeholder(R.drawable.ic_img)
//                    .into(capturedimg);
//        }
//        else {
//
//            capturedimg.setVisibility(View.GONE);
//        }
//
//        if(entertainments.getDsVideo().contains(".mp4")) {
//
//            MediaController mediaController = new MediaController(EntertainDetailActivity.this);
//            videoview.setVideoURI(Uri.parse(entertainments.getDsVideo()));
//            mediaController.setAnchorView(videoview);
//            videoview.setMediaController(mediaController);
//            videoview.start();
//        }
//        else {
//            videoview.setVisibility(View.GONE);
//            relvideo.setVisibility(View.GONE);
//        }


        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                onBackPressed();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ActivityCompat.finishAfterTransition(this);
    }
}
