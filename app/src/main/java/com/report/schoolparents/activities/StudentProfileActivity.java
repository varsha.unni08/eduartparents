package com.report.schoolparents.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.report.schoolparents.R;
import com.report.schoolparents.pojo.StudentProfile;
import com.report.schoolparents.webservice.RetrofitHelper;

public class StudentProfileActivity extends AppCompatActivity {

    ImageView imgback,imgprofile;

    TextView txtName,txtAdmission,txtAdhar,txtReligion;

    StudentProfile studentProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_profile);
        getSupportActionBar().hide();


        studentProfile=(StudentProfile) getIntent().getSerializableExtra("StudentProfile");
        imgback=findViewById(R.id.imgback);
        txtName=findViewById(R.id.txtName);
        txtAdmission=findViewById(R.id.txtAdmission);
        txtAdhar=findViewById(R.id.txtAdhar);
        txtReligion=findViewById(R.id.txtReligion);

        imgprofile=findViewById(R.id.imgprofile);




        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        getStudentProfile();

    }

    @Override
    public void onBackPressed() {
        ActivityCompat.finishAfterTransition(this);
    }

    public void getStudentProfile()
    {
        txtName.setText(studentProfile.getUdcStudentName()+" "+studentProfile.getUdcStudentLastName());
        txtAdmission.setText(studentProfile.getUdcStudentAdmissionNo());
        txtAdhar.setText(studentProfile.getUdcStudentAadarNo());
        txtReligion.setText(studentProfile.getUdcStudentReligion());

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);



        int width = displayMetrics.widthPixels;

        double heght = width / 1;
        int h = (int) heght;

        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) imgprofile.getLayoutParams();

        layoutParams.width = width;
        layoutParams.height = h;

//        layoutParams.setMargins(margin, margin, margin, margin);
        imgprofile.setLayoutParams(layoutParams);

        Glide.with(StudentProfileActivity.this)
                .load(
                        RetrofitHelper.imageBaseurl+ studentProfile.getUdcStudentPhotoPath()
                )

                .placeholder(R.drawable.ic_launcher)
                .into(imgprofile);

    }
}
