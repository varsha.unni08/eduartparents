package com.report.schoolparents.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.report.schoolparents.AppLiterals.Constants;
import com.report.schoolparents.R;
import com.report.schoolparents.adapters.EntertainmentAdapter;
import com.report.schoolparents.adapters.OnlineClassAdapter;
import com.report.schoolparents.connectivity.NetConnection;
import com.report.schoolparents.pojo.EntertainmentStatus;
import com.report.schoolparents.pojo.OnlineClassStatus;
import com.report.schoolparents.pojo.StudentProfile;
import com.report.schoolparents.pojo.StudentProfileStatus;
import com.report.schoolparents.progress.ProgressFragment;
import com.report.schoolparents.webservice.RetrofitHelper;
import com.report.schoolparents.webservice.WebInterfaceimpl;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class OnlineClassActivity extends AppCompatActivity {

    ImageView imgback,imgselectstudent;

    RecyclerView recyclerview;
    String studentid="";

    ProgressFragment progressFragment;
    TextView txtStudentSelection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_class);

        getSupportActionBar().hide( );
        imgback=findViewById(R.id.imgback);
        recyclerview=findViewById(R.id.recyclerview);

        imgselectstudent=findViewById(R.id.imgselectstudent);
        txtStudentSelection=findViewById(R.id.txtStudentSelection);



        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });


//        imgselectstudent.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                getStudentList();
//
//            }
//        });
//
//        txtStudentSelection.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                getStudentList();
//
//            }
//        });

        if(Constants.studentprofile!=null)
        {
            txtStudentSelection.setText("Selected student : "+Constants.studentprofile.getUdcStudentName());

            studentid=Constants.studentprofile.getUdcStudentId();
            getOnlineClass();
        }

    }


    public void getStudentList()
    {

        progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"cjkk");

        Retrofit retrofitHelper= RetrofitHelper.getRetrofitInstance(OnlineClassActivity.this);

        WebInterfaceimpl webInterfaceimpl=retrofitHelper.create(WebInterfaceimpl.class);

        Call<StudentProfileStatus> listCall=webInterfaceimpl.getStudentProfile();
        listCall.enqueue(new Callback<StudentProfileStatus>() {
            @Override
            public void onResponse(Call<StudentProfileStatus> call, Response<StudentProfileStatus> response) {

                progressFragment.dismiss();

                if(response.body()!=null) {

                    if(response.body().getData().size()>0) {

                        showStudentList(response.body().getData());
                    }
                    else {

                        Toast.makeText(OnlineClassActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                    }

                }

                else {

                    Toast.makeText(OnlineClassActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<StudentProfileStatus> call, Throwable t) {

                progressFragment.dismiss();


                if(!NetConnection.isConnected(OnlineClassActivity.this))
                {
                    Toast.makeText(OnlineClassActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }


            }
        });


    }


    public void showStudentList(final List<StudentProfile> students)
    {

        List<String>strings=new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(OnlineClassActivity.this);
        builder.setTitle("Choose a student");
        ;

        for (StudentProfile student:students
        ) {

            strings.add(student.getUdcStudentName());

        }



        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtStudentSelection.setText(students.get(which).getUdcStudentName());

                studentid=students.get(which).getUdcStudentId();

                // showLessonPlanlist();

                getOnlineClass();

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    public void getOnlineClass()
    {

        if(!studentid.equalsIgnoreCase("")) {

            progressFragment = new ProgressFragment();
            progressFragment.show(getSupportFragmentManager(), "dklk");
            WebInterfaceimpl webInterfaceimpl = RetrofitHelper.getRetrofitInstance(OnlineClassActivity.this).create(WebInterfaceimpl.class);

            Call<OnlineClassStatus> listCall = webInterfaceimpl.viewOnlineClass(studentid);
            listCall.enqueue(new Callback<OnlineClassStatus>() {
                @Override
                public void onResponse(Call<OnlineClassStatus> call, Response<OnlineClassStatus> response) {
                    progressFragment.dismiss();

                    if(response.body()!=null)
                    {

                        if(response.body().getData()!=null) {
                            if (response.body().getData().size() > 0) {

                                AnimationSet set = new AnimationSet(true);


                                recyclerview.setVisibility(View.VISIBLE);


                                Animation animation = new AlphaAnimation(0.0f, 1.0f);
                                animation.setDuration(500);
                                set.addAnimation(animation);

                                animation = new TranslateAnimation(
                                        Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
                                        Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF, 0.0f
                                );
                                animation.setDuration(100);
                                set.addAnimation(animation);

                                LayoutAnimationController controller = new LayoutAnimationController(set, 0.5f);

                                recyclerview.setLayoutManager(new LinearLayoutManager(OnlineClassActivity.this));

                                recyclerview.setLayoutAnimation(controller);
                                recyclerview.setAdapter(new OnlineClassAdapter(OnlineClassActivity.this, response.body().getData()));


                            } else {

                                recyclerview.setVisibility(View.GONE);
                                Toast.makeText(OnlineClassActivity.this, "No data found", Toast.LENGTH_SHORT).show();

                            }

                        }
                        else{

                            recyclerview.setVisibility(View.GONE);
                            Toast.makeText(OnlineClassActivity.this, "No data found", Toast.LENGTH_SHORT).show();
                        }


                    }
                    else {

                        recyclerview.setVisibility(View.GONE);
                        Toast.makeText(OnlineClassActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<OnlineClassStatus> call, Throwable t) {

                    progressFragment.dismiss();


                    if(!NetConnection.isConnected(OnlineClassActivity.this))
                    {
                        Toast.makeText(OnlineClassActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }
        else {

            Toast.makeText(OnlineClassActivity.this,"select a student",Toast.LENGTH_SHORT).show();

        }


    }
}