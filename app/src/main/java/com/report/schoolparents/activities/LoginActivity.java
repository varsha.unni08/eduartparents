package com.report.schoolparents.activities;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.report.schoolparents.R;
import com.report.schoolparents.appconstants.Literals;
import com.report.schoolparents.connectivity.NetConnection;
import com.report.schoolparents.pojo.LoginToken;
import com.report.schoolparents.preferencehelper.PreferenceHelper;
import com.report.schoolparents.progress.ProgressFragment;
import com.report.schoolparents.webservice.RetrofitHelperLogin;
import com.report.schoolparents.webservice.WebInterfaceimpl;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    Button btnlogin;

    EditText edtPhone,edtPassword;

    ProgressFragment progressFragment;

    ImageView imageView2;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();
        btnlogin=this.findViewById(R.id.btnlogin);
        edtPhone=this.findViewById(R.id.edtPhone);
        edtPassword=this.findViewById(R.id.edtPassword);
        imageView2=findViewById(R.id.imageView2);



        imageView2.setAlpha(0f);
        imageView2.setVisibility(View.VISIBLE);

        imageView2.animate()
                .alpha(1f)
                .setDuration(3000)
                .setListener(null);



        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!edtPhone.getText().toString().equals(""))
                {

                    if(!edtPassword.getText().toString().equals(""))
                    {

                        progressFragment=new ProgressFragment();
                        progressFragment.show(getSupportFragmentManager(),"cjkk");


                        WebInterfaceimpl webInterfaceimpl= RetrofitHelperLogin.getRetrofitInstance().create(WebInterfaceimpl.class);
                        Call<LoginToken> jsonObjectCall=webInterfaceimpl.getLoginData(edtPhone.getText().toString(),edtPassword.getText().toString());
                        jsonObjectCall.enqueue(new Callback<LoginToken>() {
                            @Override
                            public void onResponse(Call<LoginToken> call, Response<LoginToken> response) {
                                progressFragment.dismiss();

                                if(response.body().getStatus()==1)
                                {

                                    new PreferenceHelper(LoginActivity.this).putData(Literals.Logintokenkey,response.body().getToken());


                                    new PreferenceHelper(LoginActivity.this).putBooleanData(Literals.isRegistered,true);

                                    startActivity(new Intent(LoginActivity.this,HomeActivity.class));
                                    finish();

                                }
                                else {

                                    Toast.makeText(LoginActivity.this,response.body().getMessage(),Toast.LENGTH_SHORT).show();

                                }






                            }

                            @Override
                            public void onFailure(Call<LoginToken> call, Throwable t) {
                                progressFragment.dismiss();


                                if(!NetConnection.isConnected(LoginActivity.this))
                                {
                                    Toast.makeText(LoginActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                                }


                            }
                        });



                    }
                    else {

                        Toast.makeText(LoginActivity.this,"Enter password",Toast.LENGTH_SHORT).show();
                    }

                }
                else {

                    Toast.makeText(LoginActivity.this,"Enter phone",Toast.LENGTH_SHORT).show();
                }
            }
        });

    }




}
