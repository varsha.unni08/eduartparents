package com.report.schoolparents.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.google.android.material.tabs.TabLayout;
import com.report.schoolparents.R;
import com.report.schoolparents.adapters.CommunicationTabAdapter;

public class CommunicationActivity extends AppCompatActivity {

    ImageView imgback;

    TabLayout tablayout;

    ViewPager viewpager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_communication);
        getSupportActionBar().hide();

        imgback=findViewById(R.id.imgback);
        tablayout=findViewById(R.id.tablayout);
        viewpager=findViewById(R.id.viewpager);

        viewpager.setAdapter(new CommunicationTabAdapter(getSupportFragmentManager()));
        tablayout.setupWithViewPager(viewpager);

        tablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                viewpager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


       // tablayout.getTabAt(1).select();


        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        ActivityCompat.finishAfterTransition(this);
    }
}
