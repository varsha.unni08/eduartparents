package com.report.schoolparents.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.report.schoolparents.AppLiterals.Constants;
import com.report.schoolparents.R;
import com.report.schoolparents.adapters.TimeTableAdapter;
import com.report.schoolparents.connectivity.NetConnection;
import com.report.schoolparents.pojo.Period;
import com.report.schoolparents.pojo.Student;
import com.report.schoolparents.pojo.StudentProfile;
import com.report.schoolparents.pojo.StudentProfileStatus;
import com.report.schoolparents.pojo.TimeTable;
import com.report.schoolparents.pojo.TimeTableStatus;
import com.report.schoolparents.progress.ProgressFragment;
import com.report.schoolparents.webservice.RetrofitHelper;
import com.report.schoolparents.webservice.WebInterfaceimpl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class TimeTableActivity extends AppCompatActivity {

    ImageView imgback,imgselectstudent;

    RecyclerView recyclerView;

    String studentid="";
    TextView txtStudentSelection;
    ProgressFragment progressFragment;
    LinearLayout layout_days;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_table);
        getSupportActionBar().hide();
        imgback=findViewById(R.id.imgback);
        recyclerView=findViewById(R.id.recyclerView);

        txtStudentSelection=findViewById(R.id.txtStudentSelection);
        imgselectstudent=findViewById(R.id.imgselectstudent);
        layout_days=findViewById(R.id.layout_days);

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        if(Constants.studentprofile!=null)
        {
            txtStudentSelection.setText("Selected student : "+Constants.studentprofile.getUdcStudentName());

            studentid=Constants.studentprofile.getUdcStudentId();
        }

        showTimeTable();

//        txtStudentSelection.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                getStudentList();
//
//            }
//        });
//
//        imgselectstudent.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                getStudentList();
//            }
//        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ActivityCompat.finishAfterTransition(this);
    }

    public void getStudentList()
    {

        progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"cjkk");

        Retrofit retrofitHelper= RetrofitHelper.getRetrofitInstance(TimeTableActivity.this);

        WebInterfaceimpl webInterfaceimpl=retrofitHelper.create(WebInterfaceimpl.class);

        Call<StudentProfileStatus> listCall=webInterfaceimpl.getStudentProfile();
        listCall.enqueue(new Callback<StudentProfileStatus>() {
            @Override
            public void onResponse(Call<StudentProfileStatus> call, Response<StudentProfileStatus> response) {

                progressFragment.dismiss();

                if(response.body()!=null) {

                    if(response.body().getData().size()>0) {

                        showStudentList(response.body().getData());
                    }
                    else {

                        Toast.makeText(TimeTableActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                    }

                }

                else {

                    Toast.makeText(TimeTableActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<StudentProfileStatus> call, Throwable t) {

                progressFragment.dismiss();


                if(!NetConnection.isConnected(TimeTableActivity.this))
                {
                    Toast.makeText(TimeTableActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }


            }
        });


    }


    public void showStudentList(final List<StudentProfile>students)
    {

        List<String>strings=new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(TimeTableActivity.this);
        builder.setTitle("Choose a student");
        ;

        for (StudentProfile student:students
        ) {

            strings.add(student.getUdcStudentName());

        }



        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtStudentSelection.setText(students.get(which).getUdcStudentName());

                studentid=students.get(which).getUdcStudentId();

                showTimeTable();

//                showLessonPlanlist();

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    public void showTimeTable()
    {
        progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"cjkk");

        Retrofit retrofitHelper= RetrofitHelper.getRetrofitInstance(TimeTableActivity.this);

        WebInterfaceimpl webInterfaceimpl=retrofitHelper.create(WebInterfaceimpl.class);

        Call<TimeTableStatus>listCall=webInterfaceimpl.getTimetable(studentid);

        listCall.enqueue(new Callback<TimeTableStatus>() {
            @Override
            public void onResponse(Call<TimeTableStatus> call, Response<TimeTableStatus> response) {
                progressFragment.dismiss();

                if(response.body()!=null) {

                    if(response.body().getData().size()>0) {

                        recyclerView.setVisibility(View.VISIBLE);

                        Set<String>days=new HashSet<>();
                        List<Period>weekdays=new ArrayList<>();

                        String aa[]=getResources().getStringArray(R.array.days);

//                        for(String b:aa) {
//
//                            for (TimeTable tt : response.body().getData()) {
//
//                                if(b.trim().equalsIgnoreCase(tt.getUdcWeekdayName())) {
//                                    days.add(tt.getUdcWeekdayName());
//                                    break;
//                                }
//                            }
//                        }

                        for(String a : aa)
                        {
                            Period p=new Period();
                            p.setDay(a);
                            p.setSelected(0);
                            weekdays.add(p);
                        }


//                        showStudentList(response.body());

//                        List<TimeTable>tta=new ArrayList<>();
//                        tta.addAll(response.body().getData());
//                        TimeTable ttl=new TimeTable();
//                        ttl.setSelected(1);
//
//                        tta


                        recyclerView.setLayoutManager(new LinearLayoutManager(TimeTableActivity.this));
                        recyclerView.setAdapter(new TimeTableAdapter(TimeTableActivity.this,response.body().getData(),weekdays));


                    }
                    else {

                        recyclerView.setVisibility(View.GONE);

                        Toast.makeText(TimeTableActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                    }

                }

                else {

                    recyclerView.setVisibility(View.GONE);

                    Toast.makeText(TimeTableActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<TimeTableStatus> call, Throwable t) {
                progressFragment.dismiss();


                if(!NetConnection.isConnected(TimeTableActivity.this))
                {
                    Toast.makeText(TimeTableActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }



}
