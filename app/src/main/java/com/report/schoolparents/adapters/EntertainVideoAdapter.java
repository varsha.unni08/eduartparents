package com.report.schoolparents.adapters;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.report.schoolparents.R;

import java.util.List;

public class EntertainVideoAdapter extends RecyclerView.Adapter<EntertainVideoAdapter.EntertainHolder> {

    Context context;
    List<Uri>uris;

    public EntertainVideoAdapter(Context context, List<Uri> uris) {
        this.context = context;
        this.uris = uris;
    }

    public class EntertainHolder extends RecyclerView.ViewHolder{

        VideoView videoview;

        public EntertainHolder(@NonNull View itemView) {
            super(itemView);
            videoview=itemView.findViewById(R.id.videoview);
        }
    }


    @NonNull
    @Override
    public EntertainHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_entertainvideoadapter,parent,false);



        return new EntertainHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EntertainHolder holder, int position) {

        final MediaController mediacontroller = new MediaController(context);
        mediacontroller.setAnchorView(holder.videoview);

        holder.videoview.setVideoURI(uris.get(position));

        holder.videoview.setMediaController(mediacontroller);

        holder.videoview.requestFocus();
        holder.videoview.start();

    }

    @Override
    public int getItemCount() {
        return uris.size();
    }
}
