package com.report.schoolparents.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.report.schoolparents.R;
import com.report.schoolparents.pojo.LessonPlan;

import java.util.List;

public class LessonPlanAdapter extends RecyclerView.Adapter<LessonPlanAdapter.LessonPlanHolder> {


    Context context;
    List<LessonPlan>lessonPlans;

    public LessonPlanAdapter(Context context, List<LessonPlan> lessonPlans) {
        this.context = context;
        this.lessonPlans = lessonPlans;
    }

    public class LessonPlanHolder extends RecyclerView.ViewHolder{

        TextView txtdetails;

        public LessonPlanHolder(@NonNull View itemView) {
            super(itemView);

            txtdetails=itemView.findViewById(R.id.txtdetails);
        }
    }


    @NonNull
    @Override
    public LessonPlanHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_lessonplan,parent,false);

        return new LessonPlanHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LessonPlanHolder holder, int position) {


        holder.txtdetails.setText("Subject : "+lessonPlans.get(position).getUdcSubject()+"\nTopic : "+lessonPlans.get(position).getUdcLessonplanTopic()+"\nDescription : "+lessonPlans.get(position).getUdcLessonplanDescription()+"\nTeacher : "+lessonPlans.get(position).getUdcTeacherName());




//        holder.txtdetails.setText("Subject : Maths"+"\n"+"Topic : pythagoras theorem"+"\n"+"Teacher : John");

    }

    @Override
    public int getItemCount() {
        return lessonPlans.size();
    }
}
