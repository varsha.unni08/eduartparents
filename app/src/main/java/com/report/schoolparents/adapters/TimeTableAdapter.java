package com.report.schoolparents.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.report.schoolparents.R;
import com.report.schoolparents.pojo.Period;
import com.report.schoolparents.pojo.TimeTable;

import java.util.ArrayList;
import java.util.List;

public class TimeTableAdapter extends RecyclerView.Adapter<TimeTableAdapter.TimeTableHolder> {


    Context context;
    List<TimeTable>timeTables;
    List<Period>a;



    public TimeTableAdapter(Context context, List<TimeTable> timeTables,List<Period>a) {
        this.context = context;
        this.timeTables = timeTables;
        this.a=a;
    }

    public class TimeTableHolder extends RecyclerView.ViewHolder{

        TextView txtdetails;

        RecyclerView recycler_view;

        AppCompatImageView imgDropdown;
        LinearLayout layout_head;

        public TimeTableHolder(@NonNull View itemView) {
            super(itemView);

            txtdetails=itemView.findViewById(R.id.txtdetails);
            recycler_view=itemView.findViewById(R.id.recycler_view);
            imgDropdown=itemView.findViewById(R.id.imgDropdown);
            layout_head=itemView.findViewById(R.id.layout_head);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(a.get(getAdapterPosition()).getSelected()==0)
                    {
                        for (Period timeTable:a
                             ) {
                            timeTable.setSelected(0);

                        }

                        a.get(getAdapterPosition()).setSelected(1);


                    }
                    else {

                        for (Period timeTable:a
                        ) {
                            timeTable.setSelected(0);

                        }
                    }

                    notifyItemRangeChanged(0,a.size());

                }
            });
        }
    }


    @NonNull
    @Override
    public TimeTableHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_timetableadapter,parent,false);


        return new TimeTableHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TimeTableHolder holder, int position) {

        holder.txtdetails.setText(a.get(position).getDay());

        if(a.get(position).getSelected()==1)
        {
            holder.recycler_view.setVisibility(View.VISIBLE);
            holder.layout_head.setVisibility(View.VISIBLE);
            holder.imgDropdown.setImageResource(R.drawable.ic_arrow_drop_up);




//            List<Period>periods=new ArrayList<>();
//
//            Period period1=new Period();
//            period1.setPeriodname("Period 1 :"+ timeTables.get(position).getPeriod1());
//            periods.add(period1);
//            Period period2=new Period();
//            period2.setPeriodname( "Period 2 :"+timeTables.get(position).getPeriod2());
//            periods.add(period2);
//            Period period3=new Period();
//            period3.setPeriodname( "Period 3 :"+timeTables.get(position).getPeriod3());
//            periods.add(period3);
//            Period period4=new Period();
//            period4.setPeriodname( "Period 4 :"+timeTables.get(position).getPeriod4());
//            periods.add(period4);
//            Period period5=new Period();
//            period5.setPeriodname( "Period 5 :"+timeTables.get(position).getPeriod5());
//            periods.add(period5);
//            Period period6=new Period();
//            period6.setPeriodname( "Period 6 :"+timeTables.get(position).getPeriod6());
//            periods.add(period6);
//            Period period7=new Period();
//            period7.setPeriodname( "Period 7 :"+timeTables.get(position).getPeriod7());
//            periods.add(period7);
//
//
            holder.recycler_view.setLayoutManager(new LinearLayoutManager(context));

            List<TimeTable>ttl=new ArrayList<>();

            for(TimeTable t:timeTables)
            {
                if(t.getUdcWeekdayName().equalsIgnoreCase(a.get(position).getDay()))
                {

                    ttl.add(t);

                }

            }


            holder.recycler_view.setLayoutManager(new LinearLayoutManager(context));
            holder.recycler_view.setAdapter(new PeriodAdapter(context,ttl));

        }
        else {

            holder.recycler_view.setVisibility(View.GONE);
            holder.layout_head.setVisibility(View.GONE);
            holder.imgDropdown.setImageResource(R.drawable.ic_arrow_drop_down);
        }

    }

    @Override
    public int getItemCount() {
        return a.size();
    }
}
