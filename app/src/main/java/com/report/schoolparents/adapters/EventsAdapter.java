package com.report.schoolparents.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.report.schoolparents.R;
import com.report.schoolparents.pojo.Events;

import java.util.List;

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.EventsHolder> {


    Context context;
    List<Events>events;

    public EventsAdapter(Context context, List<Events> events) {
        this.context = context;
        this.events = events;
    }

    public class EventsHolder extends RecyclerView.ViewHolder{

        TextView txt1;

        public EventsHolder(@NonNull View itemView) {
            super(itemView);
            txt1=itemView.findViewById(R.id.txt1);
        }
    }

    @NonNull
    @Override
    public EventsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater=LayoutInflater.from(parent.getContext());
        View view=layoutInflater.inflate(R.layout.layout_attendance,parent,false);
        return new EventsHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EventsHolder holder, final int position) {

        if(events.get(position).getIndex()!=-1)
        {


            holder.txt1.setText(events.get(position).getData());

            if(!events.get(position).getUdc_event_date().equalsIgnoreCase("")) {


                Typeface face = Typeface.createFromAsset(context.getAssets(),
                        "fonts/montserratsemibold.otf");

                holder.txt1.setTypeface(face);

                holder.txt1.setTextColor(Color.WHITE);
                holder.txt1.setBackgroundColor(Color.parseColor("#4AC6F7"));

                holder.txt1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        StringBuilder sb=new StringBuilder();
                        sb.append("Event name : "+events.get(position).getUdcEventName()+"\n");
                        sb.append("Description : "+events.get(position).getUdcEventDescription()+"\n");
                        sb.append("Date : "+events.get(position).getUdc_event_date());



                        AlertDialog.Builder builder=new AlertDialog.Builder(context);
                        builder.setTitle(R.string.app_name);
                        builder.setMessage(sb.toString());
                        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();

                            }
                        });
                        builder.show();

                    }
                });

            }
        }
        else {



            holder.txt1.setText(events.get(position).getData());
        }

    }

    @Override
    public int getItemCount() {
        return events.size();
    }
}
