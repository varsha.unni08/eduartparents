package com.report.schoolparents.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.report.schoolparents.R;
import com.report.schoolparents.pojo.Circular;

import java.util.List;

public class CircularAdapter  extends RecyclerView.Adapter<CircularAdapter.CircularHolder> {

    Context context;
    List<Circular>circulars;

    public CircularAdapter(Context context, List<Circular> circulars) {
        this.context = context;
        this.circulars = circulars;
    }

    public class CircularHolder extends RecyclerView.ViewHolder{

        TextView txtdetails;

        public CircularHolder(@NonNull View itemView) {
            super(itemView);

            txtdetails=itemView.findViewById(R.id.txtdetails);
        }
    }


    @NonNull
    @Override
    public CircularHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_lessonplan,parent,false);


        return new CircularHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CircularHolder holder, int position) {

        holder.txtdetails.setText(circulars.get(position).getUdcCircularHead()+"\n"+circulars.get(position).getUdcCircularContent()+"\n"+circulars.get(position).getUdcDatePrep());

    }

    @Override
    public int getItemCount() {
        return circulars.size();
    }
}
