package com.report.schoolparents.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.report.schoolparents.R;
import com.report.schoolparents.pojo.Leave;

import java.util.List;

public class LeaveAdapter extends RecyclerView.Adapter<LeaveAdapter.LeaveHolder> {

    Context context;
    List<Leave>leaves;

    public LeaveAdapter(Context context, List<Leave> leaves) {
        this.context = context;
        this.leaves = leaves;
    }

    public class LeaveHolder extends RecyclerView.ViewHolder{

        TextView txtdetails;

        public LeaveHolder(@NonNull View itemView) {
            super(itemView);

            txtdetails=itemView.findViewById(R.id.txtdetails);
        }
    }

    @NonNull
    @Override
    public LeaveHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_lessonplan,parent,false);


        return new LeaveHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LeaveHolder holder, int position) {

        holder.txtdetails.setText(leaves.get(position).getUdcReason()+"\n\nFrom "+leaves.get(position).getUdcFromDate()+" to "+leaves.get(position).getUdcToDate());

    }

    @Override
    public int getItemCount() {
        return leaves.size();
    }
}
