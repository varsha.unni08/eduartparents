package com.report.schoolparents.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.report.schoolparents.R;
import com.report.schoolparents.pojo.MarkList;

import java.util.List;

public class MarklistAdapter extends RecyclerView.Adapter<MarklistAdapter.Marklistholder> {

    Context context;
    List<MarkList>markLists;

    public MarklistAdapter(Context context, List<MarkList> markLists) {
        this.context = context;
        this.markLists = markLists;
    }

    public class Marklistholder extends RecyclerView.ViewHolder
    {

        TextView txtSubject,txtMarks,txtTotalMarks,txtReview;

        public Marklistholder(@NonNull View itemView) {
            super(itemView);

            txtSubject=itemView.findViewById(R.id.txtSubject);
            txtMarks=itemView.findViewById(R.id.txtMarks);
            txtTotalMarks=itemView.findViewById(R.id.txtTotalMarks);
            txtReview=itemView.findViewById(R.id.txtReview);
        }
    }

    @NonNull
    @Override
    public Marklistholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_marklistholder,parent,false);


        return new Marklistholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Marklistholder holder, int position) {

        holder.txtSubject.setText(markLists.get(position).getSubject());
        holder.txtMarks.setText(markLists.get(position).getMarksScored());
        holder.txtTotalMarks.setText(markLists.get(position).getTotalMarks());
        holder.txtReview.setText(markLists.get(position).getTreview());

    }

    @Override
    public int getItemCount() {
        return markLists.size();
    }
}
