package com.report.schoolparents.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.report.schoolparents.R;
import com.report.schoolparents.pojo.Message;

import java.util.List;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageHolder> {

    Context context;
    List<Message>messages;

    public MessageAdapter(Context context, List<Message> messages) {
        this.context = context;
        this.messages = messages;
    }

    public class MessageHolder extends RecyclerView.ViewHolder{

        TextView txtdetails;

        public MessageHolder(@NonNull View itemView) {
            super(itemView);

            txtdetails=itemView.findViewById(R.id.txtdetails);
        }
    }

    @NonNull
    @Override
    public MessageHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_lessonplan,parent,false);

        return new MessageHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MessageHolder holder, int position) {

        holder.txtdetails.setText(messages.get(position).getUdcMessageTopic()+"\n"+messages.get(position).getUdcMessageDescription()+"\n Sent by "+messages.get(position).getUdc_teacher_name()+"\n"+messages.get(position).getUdcMessageDate());

    }

    @Override
    public int getItemCount() {
        return messages.size();
    }
}
