package com.report.schoolparents.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.report.schoolparents.R;
import com.report.schoolparents.pojo.Attendance;

import java.util.List;
import java.util.Map;

public class AttendanceAdapter extends RecyclerView.Adapter<AttendanceAdapter.Attendanceholder> {

    Context context;

   List<Attendance>attendances;

    public AttendanceAdapter(Context context, List<Attendance> attendances) {
        this.context = context;
        this.attendances = attendances;
    }

    public class Attendanceholder extends RecyclerView.ViewHolder
    {

        TextView txt1;

        ImageView img;

        public Attendanceholder(@NonNull View itemView) {
            super(itemView);
            txt1=itemView.findViewById(R.id.txt1);
            img=itemView.findViewById(R.id.img);

        }
    }


    @NonNull
    @Override
    public Attendanceholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater=LayoutInflater.from(parent.getContext());
        View view=layoutInflater.inflate(R.layout.layout_attendance,parent,false);

        return new Attendanceholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Attendanceholder holder, final int position) {

        if(attendances.get(position).getIndex()!=-1)
        {

            if(attendances.get(position).isIsleave()) {

                Typeface face = Typeface.createFromAsset(context.getAssets(),
                        "fonts/montserratsemibold.otf");

                holder.txt1.setTypeface(face);


                holder.img.setBackgroundResource(R.drawable.ic_close);
                holder.img.setVisibility(View.VISIBLE);


                holder.txt1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        StringBuilder sb=new StringBuilder();
                        sb.append("Leave date : "+attendances.get(position).getUdcAbsenceDate()+"\n\n");

                        sb.append("Reason : "+attendances.get(position).getUdcComments());



                        AlertDialog.Builder builder=new AlertDialog.Builder(context);
                        builder.setTitle(R.string.app_name);
                        builder.setMessage(sb.toString());
                        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();

                            }
                        });
                        builder.show();

                    }
                });





            }


            holder.txt1.setText(attendances.get(position).getData());
        }
        else {


            holder.txt1.setText(attendances.get(position).getData());
        }

    }

    @Override
    public int getItemCount() {
        return attendances.size();
    }
}
