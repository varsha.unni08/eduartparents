package com.report.schoolparents.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.report.schoolparents.R;
import com.report.schoolparents.pojo.Installments;

import java.util.List;

public class InstallmentAdapter extends RecyclerView.Adapter<InstallmentAdapter.InstallmentHolder> {

    Context context;
    List<Installments>installments;

    public InstallmentAdapter(Context context,List<Installments>installments) {
        this.context = context;
        this.installments=installments;
    }

    public  class  InstallmentHolder extends RecyclerView.ViewHolder {

        TextView txtName,txtStatus,txtAmount;
        Button btnpay;

        public InstallmentHolder(@NonNull View itemView) {
            super(itemView);
            txtAmount=itemView.findViewById(R.id.txtAmount);
            txtName=itemView.findViewById(R.id.txtName);
            txtStatus=itemView.findViewById(R.id.txtStatus);
            btnpay=itemView.findViewById(R.id.btnpay);
        }

    }


    @NonNull
    @Override
    public InstallmentHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_installment, parent, false);

        return new InstallmentHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull InstallmentHolder holder, int position) {

        holder.txtAmount.setText(installments.get(position).getFees()+" Rs");
        holder.txtName.setText(installments.get(position).getFeename()+" ")  ;

        if(installments.get(position).getStatus()==0) {
            holder.txtStatus.setText( "Pending ");
            holder.txtStatus.setTextColor(Color.parseColor("#db5860"));

            holder.btnpay.setVisibility(View.VISIBLE);
        }
        else{
            holder.txtStatus.setText("Paid ");
            holder.txtStatus.setTextColor(Color.parseColor("#59a869"));
            holder.btnpay.setVisibility(View.GONE);

        }

    }

    @Override
    public int getItemCount() {
        return installments.size();
    }
}
