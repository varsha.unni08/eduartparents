package com.report.schoolparents.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.report.schoolparents.R;
import com.report.schoolparents.activities.EntertainDetailActivity;
import com.report.schoolparents.pojo.Entertainments;

import java.util.List;

public class EntertainmentAdapter extends RecyclerView.Adapter<EntertainmentAdapter.EntertainHolder> {


    Context context;
    List<Entertainments>entertainments;

    public EntertainmentAdapter(Context context,List<Entertainments>entertainments) {
        this.context = context;
        this.entertainments=entertainments;
    }

    public class EntertainHolder extends RecyclerView.ViewHolder{

        ImageView imgEntertain;
        TextView txtname,txtDescription;

        TextView txtPhotolink,txtVideolink;

        public EntertainHolder(@NonNull View itemView) {
            super(itemView);
            imgEntertain=itemView.findViewById(R.id.imgEntertain);
            txtname=itemView.findViewById(R.id.txtname);
            txtDescription=itemView.findViewById(R.id.txtDescription);

            txtPhotolink=itemView.findViewById(R.id.txtPhotolink);
            txtVideolink=itemView.findViewById(R.id.txtVideolink);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent=new Intent(context, EntertainDetailActivity.class);
                    intent.putExtra("Entertainment",entertainments.get(getAdapterPosition()));
                    //context.startActivity(intent);

                    ActivityOptionsCompat options =

                            ActivityOptionsCompat.makeSceneTransitionAnimation((Activity) context,
                                    view,   // Starting view
                                    context.getString(R.string.transition)    // The String
                            );

                    ActivityCompat.startActivity(context, intent, options.toBundle());
                }
            });
        }
    }

    @NonNull
    @Override
    public EntertainHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_entertainadapter,parent,false);


        return new EntertainHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EntertainHolder holder, int position) {


        if(entertainments.get(position).getDsImage()!=null) {
            Glide.with(context)
                    .load(entertainments.get(position).getDsImage())

                    .placeholder(R.drawable.ic_launcher)
                    .into(holder.imgEntertain);
        }
        holder.
                txtname.setText(entertainments.get(position).getDsEntertainmentName());
        holder.txtDescription.setText(entertainments.get(position).getDsEntertainmentDescription());

        holder.txtPhotolink.setText(entertainments.get(position).getDsImage());

        holder.txtVideolink.setText(entertainments.get(position).getDsVideo());

    }

    @Override
    public int getItemCount() {
        return entertainments.size();
    }
}
