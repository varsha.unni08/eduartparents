package com.report.schoolparents.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.report.schoolparents.R;
import com.report.schoolparents.activities.StudentProfileActivity;
import com.report.schoolparents.pojo.Student;
import com.report.schoolparents.pojo.StudentProfile;
import com.report.schoolparents.webservice.RetrofitHelper;

import java.util.List;


public class StudentsAdapter extends PagerAdapter {

    Context context;
    List<StudentProfile>students;

    public StudentsAdapter(Context context, List<StudentProfile>students) {
        this.context=context;
        this.students=students;
    }

    @Override
    public int getCount() {
        return students.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {

        LayoutInflater mLayoutInflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        View itemView = mLayoutInflater.inflate(R.layout.pager_item, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
        TextView txtname=itemView.findViewById(R.id.txtname);

//        if (position==0) {
//            Glide.with(context)
//                    .load("https://completeconcussions.com/drive/uploads/2017/10/detail-john-doe.jpg")
//                    .apply(RequestOptions.circleCropTransform())
//                    .placeholder(R.drawable.ic_launcher)
//                    .into(imageView);
//            txtname.setText("Antony k j");
//        }
//        else {

            Glide.with(context)
                    .load(
                            RetrofitHelper.imageBaseurl+students.get(position).getUdcStudentPhotoPath()
                    )
                    .apply(RequestOptions.circleCropTransform())
                    .placeholder(R.drawable.ic_launcher)
                    .into(imageView);
            txtname.setText(students.get(position).getUdcStudentName()+" "+students.get(position).getUdcStudentLastName());


        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(context, StudentProfileActivity.class);
                intent.putExtra("StudentProfile",students.get(position));

              //  context.startActivity(intent);


                String transitionName = context.getString(R.string.transition);


                ActivityOptionsCompat options =

                        ActivityOptionsCompat.makeSceneTransitionAnimation((Activity) context,
                                view,   // Starting view
                                transitionName    // The String
                        );

                ActivityCompat.startActivity(context, intent, options.toBundle());


            }
        });

        container.addView(itemView);

        return itemView;
    }
}
