package com.report.schoolparents.adapters;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.report.schoolparents.fragments.MessageFragment;
import com.report.schoolparents.fragments.CircularFragment;
import com.report.schoolparents.fragments.ComplaintFragment;
import com.report.schoolparents.fragments.LeaveFragment;

public class CommunicationTabAdapter extends FragmentPagerAdapter {

    String arr[]={"Leave","Outbox","Circular","Messages"};


    public CommunicationTabAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment=null;

        if(position==0)
        {

            fragment=new LeaveFragment();
        }
        else if(position==1) {

            fragment=new ComplaintFragment();

        }
        else if(position==2) {

            fragment=new CircularFragment();

        }

        else if(position==3) {

            fragment=new MessageFragment();

        }



        return fragment;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return arr[position];
    }
}
