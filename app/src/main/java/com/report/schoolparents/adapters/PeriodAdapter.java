package com.report.schoolparents.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.report.schoolparents.R;
import com.report.schoolparents.pojo.Period;
import com.report.schoolparents.pojo.TimeTable;

import java.util.List;

public class PeriodAdapter extends RecyclerView.Adapter<PeriodAdapter.PeriodHolder> {


    Context context;
    List<TimeTable>periods;

    public PeriodAdapter(Context context, List<TimeTable> periods) {
        this.context = context;
        this.periods = periods;
    }

    public class PeriodHolder extends RecyclerView.ViewHolder{

        TextView txtudcperiod,txtsubject,txtteacher;

        public PeriodHolder(@NonNull View itemView) {
            super(itemView);
            txtudcperiod=itemView.findViewById(R.id.txtudcperiod);
            txtsubject=itemView.findViewById(R.id.txtsubject);
            txtteacher=itemView.findViewById(R.id.txtteacher);
        }
    }

    @NonNull
    @Override
    public PeriodHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_periodadapter,parent,false);


        return new PeriodHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PeriodHolder holder, int position) {

//        holder.txtperiod.setText(periods.get(position).getDay());

        holder.txtudcperiod.setText(periods.get(position).getUdcPeriod());
        holder.txtsubject.setText(periods.get(position).getUdcSubjectDetails());
        holder.txtteacher.setText(periods.get(position).getUdcTeacherName());

    }

    @Override
    public int getItemCount() {
        return periods.size();
    }
}
