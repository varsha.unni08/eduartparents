package com.report.schoolparents.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.report.schoolparents.R;
import com.report.schoolparents.pojo.OnlineClass;

import java.util.List;

public class OnlineClassAdapter extends RecyclerView.Adapter<OnlineClassAdapter.OnlineClassHolder> {


    Context context;
    List<OnlineClass>onlineClasses;

    public OnlineClassAdapter(Context context, List<OnlineClass> onlineClasses) {
        this.context = context;
        this.onlineClasses = onlineClasses;
    }

    public class OnlineClassHolder extends RecyclerView.ViewHolder {
        TextView txtName,txtStatus,txtAmount;

        public OnlineClassHolder(@NonNull View itemView) {
            super(itemView);

            txtAmount=itemView.findViewById(R.id.txtAmount);
            txtName=itemView.findViewById(R.id.txtName);
            txtStatus=itemView.findViewById(R.id.txtStatus);
        }
    }


    @NonNull
    @Override
    public OnlineClassHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater=LayoutInflater.from(parent.getContext());
        View view=layoutInflater.inflate(R.layout.layout_onlineclass,parent,false);


        return new OnlineClassHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OnlineClassHolder holder, int position) {

        holder.txtName.setText(onlineClasses.get(position).getClasstopic());
        holder.txtAmount.setText(onlineClasses.get(position).getTime());
        holder.txtStatus.setText(onlineClasses.get(position).getJoinlink());

    }

    @Override
    public int getItemCount() {
        return onlineClasses.size();
    }
}
