package com.report.schoolparents.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.report.schoolparents.R;
import com.report.schoolparents.activities.AddEntertainActivity;

import java.io.File;
import java.util.List;

public class EnterTainImageAdapter extends RecyclerView.Adapter<EnterTainImageAdapter.Entertainimgholder> {


    Context context;
    List<File> files_images;

    public EnterTainImageAdapter(Context context,List<File> files_images) {
        this.context = context;
        this.files_images=files_images;
    }

    public class Entertainimgholder extends RecyclerView.ViewHolder
    {

        ImageView imgentertain,imgdelete;

        public Entertainimgholder(@NonNull View itemView) {
            super(itemView);
            imgentertain=itemView.findViewById(R.id.imgentertain);
            imgdelete=itemView.findViewById(R.id.imgdelete);

            imgdelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                   // ((AddEntertainActivity)context).refreshImageAdapter(getAdapterPosition());

                }
            });
        }
    }


    @NonNull
    @Override
    public Entertainimgholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_entertainimg,parent,false);



        return new Entertainimgholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Entertainimgholder holder, int position) {

        Glide.with(context)
                .load(files_images.get(position))

                .placeholder(R.drawable.ic_launcher)
                .into(holder.imgentertain);

    }

    @Override
    public int getItemCount() {
        return files_images.size();
    }
}
